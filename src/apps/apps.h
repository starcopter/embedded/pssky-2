/**
 * @file apps.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Common Application Header
 * @version 0.1
 * @date 2021-12-03
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 */

#pragma once

#include <main.h>

#include <FreeRTOS.h>
#include <task.h>

#include <canard/node.h>

typedef enum Mode {
    // Normal operating mode.
    MODE_OPERATIONAL = 0,
    // Initialization is in progress; this mode is entered immediately after startup.
    MODE_INITIALIZATION = 1,
    // Calibration, self-test, etc.
    MODE_MAINTENANCE
} Mode;

typedef enum Health {
    // The component is functioning properly (nominal).
    HEALTH_NOMINAL = 0,
    // A critical parameter went out of range or the component encountered a minor failure that does not prevent
    // the subsystem from performing any of its real-time functions.
    HEALTH_ADVISORY = 1,
    // The component encountered a major failure and is performing in a degraded mode or outside of its designed
    // limitations.
    HEALTH_CAUTION = 2,
    // The component suffered a fatal malfunction and is unable to perform its intended function.
    HEALTH_WARNING = 3,
} Health;

typedef enum Readiness {
    READINESS_SLEEP   = 0,  // reg.udral.service.common.Readiness.0.1 SLEEP
    READINESS_STANDBY = 2,  // reg.udral.service.common.Readiness.0.1 STANDBY
    READINESS_ENGAGED = 3,  // reg.udral.service.common.Readiness.0.1 ENGAGED
    READINESS_TIMEOUT = 4,  // implicit
} Readiness;

typedef enum ChargeStatus {
    UNKNOWN  = 0, // The charger's status is not known at the MODE_MAINTENANCE
    CHARGING = 1, // The charge is in progress (including automatic charging)
    DISABLED = 2, // The cell charge is completed, or the charger is disabled or in sleep mode
    FAULT    = 3, // The charger is in fault state (include OVP, BAT OVP, TSHUT, TS, TMR_EXP)
} ChargeStatus;

struct Temperatures {
    float cell;        // under cell temperature
    float stm32_core;  // stm32 internal core temperature
    float pstage;      // ntc temperature sensor between the power stage
};

/**
 * @brief Get Temperature Measurements
 * @details The function returns the measurements from different temperature sources.
 * @note The value returned by this function is not filtered, aware of aliasing.
 * @return pointer to the structure instance
 */
const struct Temperatures* get_temperatures(void);

void start_main_task(void);
void start_node_monitor_task(void);
void start_analog_task(void);
void init_ina2xx_emulation(void);

/**
 * @brief Get Measured System Voltage (V48 net)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, V48 net voltage in volts
 */
float get_system_voltage(void);

/**
 * @brief Get External Reference Voltage (extrpolated from the internal VREFINT)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, external reference voltage measurement in Volts
 */
float get_external_reference_voltage(void);

/**
 * @brief Get STM32 supply voltage (VDD net)
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, stm32 supply voltage in volts
 */
float get_stm32_supply_voltage(void);

/**
 * @brief Get Measured STM32 Core Temperature
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, core temperature in Kelvin
 */
float get_core_temperature(void);

/**
 * @brief Get Measured Skynode Secondary Supply Voltage (VSKY net)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, VSKY net voltage in volts
 */
float get_skynode_secondary_supply_voltage(void);

/**
 * @brief Get Measured Skynode Secondary Supply Current (ISKY)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, current drawn by the Skynode from the secondary power supply (in Amperes)
 */
float get_skynode_secondary_supply_current(void);

/**
 * @brief Get cell temperature celcius
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, cell temperature in Kelvin
 */
float get_cell_temperature(void);

/**
 * @brief Get temperature sensed between power stage
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, temperature in Kelvin
 */
float get_power_stage_temperature(void);

/**
 * @brief Get buck stage output current
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, current drawn from the buck stage (in Amperes)
 */
float get_buck_stage_output_current(void);

/**
 * @brief Get boost stage output voltage (VCC net)
 * @note The value returned by this function is low-pass filtered to 4Hz and should be safe to use in most cases.
 * @return float, voltage across VCC net in volts
 */
float get_boost_stage_output_voltage(void);

/**
 * @brief Get voltage across battery (VBAT)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, voltage across VBAT net in volts
 */
float get_cell_voltage(void);

/**
 * @brief Get cell current (IBAT)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, current drawn from the cell (in Amperes)
 */
float get_cell_current(void);

/**
 * @brief Get cell charge current (ICHG)
 * @note The value returned by this function is low-pass filtered to 20Hz and should be safe to use in most cases.
 * @return float, single cell charging current (in Amperes)
 */
float get_cell_charge_current(void);
