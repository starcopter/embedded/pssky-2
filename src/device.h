/**
 * @file device.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Load Correct CMSIS Device Header
 * @date 2021-08-16
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @see https://www.keil.com/pack/doc/CMSIS/Core/html/device_h_pg.html
 */

#if defined(STM32G431xx)
#    include "stm32g4xx-extra.h"
#    include "stm32g4xx.h"
#else
#    error "This project is intended for the STM32G431RB microcontroller only"
#endif
