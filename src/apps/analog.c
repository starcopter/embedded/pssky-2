/**
 * @file analog.c
 * @author Sajeel Ahmed <sajeel@starcopter.com>, Lasse Fröhner <lasse@starcopter.com>
 * @brief ADC Initialization and Low-Level Drivers
 * @version 0.1
 * @date 2023-06-01
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * Channel Configuration
 * ---------------------
 * | ADC | Channel | Signal             | Sequence                                | Trigger         |
 * |----:|--------:|--------------------|-----------------------------------------|-----------------|
 * |   1 |       2 | `IBUCK_ADC`        | regular `SQ1`                           | `TIM6`, 100Hz   |
 * |   1 |       3 | `ICHG_ADC`         | regular `SQ2`                           | `TIM6`, 100Hz   |
 * |   1 |       4 | `IBAT_ADC`         | regular `SQ3`                           | `TIM6`, 100Hz   |
 * |   1 |      10 | `VCC_ADC`          | regular `SQ4`                           | `TIM6`, 100Hz   |
 * |   1 |      15 | `CELL_TEMP`        | regular `SQ5`                           | `TIM6`, 100Hz   |
 * |   1 |      16 | `Core Temperature` | regular `SQ6`                           | `TIM6`, 100Hz   |
 * |   1 |      17 | `Vbat`             | regular `SQ7`                           | `TIM6`, 100Hz   |
 * |   1 |      18 | `VREFINT`          | regular `SQ8`                           | `TIM6`, 100Hz   |
 * |   2 |       1 | `ISKY_ADC`         | regular `SQ1`                           | `TIM6`, 100Hz   |
 * |   2 |       3 | `V48_ADC`          | regular `SQ2`                           | `TIM6`, 100Hz   |
 * |   2 |       4 | `VSKY_ADC`         | regular `SQ3`                           | `TIM6`, 100Hz   |
 * |   2 |      10 | `PSTAGE_TEMP`      | regular `SQ4`                           | `TIM6`, 100Hz   |
 * |   2 |      13 | `VBAT_ADC`         | regular `SQ5`                           | `TIM6`, 100Hz   |
 *
 */

#include "apps.h"
#include <math.h>

#include <canard/node.h>
#include <sclib/assert.h>
#include <sclib/registry.h>
#include <sclib/timekeeper.h>
#include <sclib/dsp.h>

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>
#include <timers.h>

// bits are defined to represent each interrupt source.
#define DMA2_TCIF5 0x01
#define DMA2_TCIF6 0x02

BiquadFilterHandle_t vref_20Hz;  // analog VREF+ reference voltage, updated from extrapolating VREFINT

BiquadFilterHandle_t V48_20Hz;  // V48, filtered down to 20Hz
BiquadFilterHandle_t V48_4Hz;   // V48, filtered down to 4Hz

BiquadFilterHandle_t VSKY_20Hz;  // VSKY, filtered down to 20Hz
BiquadFilterHandle_t VSKY_4Hz;   // VSKY, filtered down to 4Hz

BiquadFilterHandle_t ISKY_20Hz;  // ISKY, filtered down to 20Hz
BiquadFilterHandle_t ISKY_4Hz;   // ISKY, filtered down to 4Hz

BiquadFilterHandle_t VCC_20Hz;  // VCC, filtered down to 20Hz
BiquadFilterHandle_t VCC_4Hz;   // VCC, filtered down to 4Hz

BiquadFilterHandle_t VBAT_20Hz;  // VBAT_ADC, filtered down to 20Hz
BiquadFilterHandle_t VBAT_4Hz;   // VBAT_ADC, filtered down to 4Hz

BiquadFilterHandle_t IBAT_20Hz;  // IBAT_ADC, filtered down to 20Hz
BiquadFilterHandle_t IBAT_4Hz;   // IBAT_ADC, filtered down to 4Hz

BiquadFilterHandle_t ICHG_20Hz;  // ICHG_ADC, filtered down to 20Hz
BiquadFilterHandle_t ICHG_4Hz;   // ICHG_ADC, filtered down to 4Hz

BiquadFilterHandle_t IBUCK_20Hz;       // IBUCK_ADC, filtered down to 20Hz
BiquadFilterHandle_t IBUCK_4Hz;        // IBUCK_ADC, filtered down to 4Hz
BiquadFilterHandle_t VDD_4Hz;          // STM32 supply voltage, filtered down to 4Hz
BiquadFilterHandle_t core_temp_4Hz;    // STM32 core temperature, filtered down to 4Hz
BiquadFilterHandle_t pstage_temp_4Hz;  // power stage temperature, filtered down to 4Hz
BiquadFilterHandle_t cell_temp_4Hz;    // cell temperature temperature, filtered down to 4Hz

// The handle of the task that will receive notifications from the interrupts.
static TaskHandle_t xHandlingTask;

const float T0  = 273.15f;  // 0 °C in Kelvin
const float T25 = T0 + 25;  // 25 °C in Kelvin

static const float INA4181A3_GAIN_FACTOR = 100.0f;  // INA4181A3 opamp, gain factor is 100V/V
static const float VREF_OFFSET_VOLTAGE   = 0.2f;    // reference offset voltage for the current amplifier (INA4181A3)
static const float V48_SCALING_FACTOR    = (1000 + 68) / 68.0f;
static const float VSKY_SCALING_FACTOR   = (100 + 100) / 100.0f;
static const float VBAT_SCALING_FACTOR   = (100 + 150) / 150.0f;
static const float VCC_SCALING_FACTOR    = (100 + 100) / 100.0f;
static const float VDD_SCALING_FACTOR    = 3.0f;  // RM0440 Rev 7 21.4.32 resistive bridge supply VBAT/3 to ADC

static const float ISKY_SCALING_FACTOR  = 1 / (INA4181A3_GAIN_FACTOR * 0.005f);  // 1 / (GAIN_FACTOR * RSENSE)
static const float IBAT_SCALING_FACTOR  = 1 / (INA4181A3_GAIN_FACTOR * 0.002f);  // 1 / (GAIN_FACTOR * RSENSE)
static const float IBUCK_SCALING_FACTOR = 1 / (INA4181A3_GAIN_FACTOR * 0.005f);  // 1 / (GAIN_FACTOR * RSENSE)
static const float ICHG_SCALING_FACTOR  = 1 / (INA4181A3_GAIN_FACTOR * 0.005f);  // 1 / (GAIN_FACTOR * RSENSE)

const int FULL_RANGE_12BIT = (1 << 12) - 1;

// 2nd order low pass filter, fc = 0.2 fs
const BiquadStage LPF_O2_F20[] = {
    {+2.065720838e-01f, +4.131441677e-01f, +2.065720838e-01f, +1.000000000e+00f, -3.695273774e-01f, +1.958157127e-01f}};
// 2nd order low pass filter, fc = 0.04 fs
const BiquadStage LPF_O2_F04[] = {
    {+1.335920003e-02f, +2.671840006e-02f, +1.335920003e-02f, +1.000000000e+00f, -1.647459981e+00f, +7.008967812e-01f}};

// DMA2 Channel 5 fills this from ADC1->DR at 100Hz
volatile struct __PACKED ADC1RawResults {
    uint16_t ibuck_adc;
    uint16_t ichg_adc;
    uint16_t ibat_adc;
    uint16_t vcc_adc;
    uint16_t cell_temp;
    uint16_t stm32_core;
    uint16_t vbat;
    uint16_t vrefint;
} adc1_raw_results;

// DMA2 Channel 6 fills this from ADC2->DR at 100Hz
volatile struct __PACKED ADC2RawResults {
    uint16_t isky_adc;
    uint16_t v48_adc;
    uint16_t vsky_adc;
    uint16_t pstage_temp;
    uint16_t vbat_adc;
} adc2_raw_results;

static struct Temperatures temperatures;

static void Analog(void* param);

/**
 * @brief Initialize ADC, Triggers and DMA.
 * @details This function configures the analog subsystem. It sets up TIM6 as trigger, enables the ADC voltage
 * regulator, performs necessary calibration steps, configures internal channel mapping, resolution and sampling times,
 * sets up DMA2 Channel 5 to manage ADC1 results, DMA2 Channel 6 to manage ADC2 results, and starts TIM6 and the ADC1's
 * and ADC2's regular sequence.
 *
 * @note It DOES NOT enable the interfaces in RCC or configure GPIO pins.
 *
 * @note This function shall be called only once.
 */
static void analog_init(void);

/**
 * @brief Calculate STM32 core temperature
 * @details Calculates the actual temperature of the stm32 using the calibration values provided by the manufacturer in
 * the datasheet. Refer to DS13122 Rev 3, Section 3.18.1 page 30 Table 5 Temperature Sensor Calibration values
 * @return float, temperature in kelvin
 */
static float measure_stm32_core_temperature(void);

/**
 * @brief Measure NTC temperature
 * @note This function defines the NTC temperature sensor characteristics and implicitly calls
 * calculate_temperature_from_NTC() to calculate temperature values as a function of change in
 * resistance.
 * @param adc_value ADC converted temperature
 * @return float, temperature in Kelvin
 */
float measure_ntc_temperature(uint16_t adc_value);

/**
 * @brief calculate temperature from an NTC resistive voltage divider
 *
 * \f$ T = \frac{1}{\ln( \frac{R_{1,\text{25}}}{R_{\text{NTC},\text{25}}} \cdot \frac{V_T}{V_\text{ref}-V_T} ) +
 * \frac{1}{T_\text{25}}} \f$
 *
 * The units of the resistance values and voltages do not matter, as long as they match each other.
 *
 * @param   R1_25       reference resistor resistance at room temperature
 * @param   RNTC_25     NTC resistance at room temperature
 * @param   VT          measured voltage output from voltage divider
 * @param   Vref        reference voltage input into voltage divider
 * @param   B2585       NTC's B parameter (B_{25/85}) in Kelvin
 * @return  float       temperature in Kelvin
 */
static float calculate_temperature_from_NTC(float R1_25, float RNTC_25, float VT, float Vref, float B2585);

// =========================== ACCESSIBLE FUNCTIONS ===========================
float get_system_voltage(void) {
    return get_sos_filter_output(V48_20Hz);
}

float get_stm32_supply_voltage(void) {
    return get_sos_filter_output(VDD_4Hz);
}

float get_external_reference_voltage(void) {
    return get_sos_filter_output(vref_20Hz);
}

float get_skynode_secondary_supply_voltage(void) {
    return get_sos_filter_output(VSKY_20Hz);
}

float get_skynode_secondary_supply_current(void) {
    return get_sos_filter_output(ISKY_20Hz);
}

float get_boost_stage_output_voltage(void) {
    return get_sos_filter_output(VCC_4Hz);
}

float get_buck_stage_output_current(void) {
    return get_sos_filter_output(IBUCK_4Hz);
}

float get_cell_voltage(void) {
    return get_sos_filter_output(VBAT_20Hz);
}

float get_cell_current(void) {
    return get_sos_filter_output(IBAT_20Hz);
}

float get_cell_charge_current(void) {
    return get_sos_filter_output(ICHG_20Hz);
}

float get_core_temperature(void) {
    return temperatures.stm32_core;
}

float get_cell_temperature(void) {
    return temperatures.cell;
}

float get_power_stage_temperature(void) {
    return temperatures.pstage;
}
const struct Temperatures* get_temperatures(void) {
    return &temperatures;
}

// =========================== FUNCTION DESCRIPTIONS ===========================

void start_analog_task(void) {
    BaseType_t status = xTaskCreate(Analog, "Analog", 512, NULL, TaskPriorityLow, &xHandlingTask);
    ASSERT(status == pdPASS);
}

static void Analog(void* param __unused) {

    vref_20Hz = init_sos_filter(LPF_O2_F20, NULL);

    V48_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    V48_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    VSKY_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    VSKY_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    ISKY_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    ISKY_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    VCC_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    VCC_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    VBAT_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    VBAT_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    IBAT_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    IBAT_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    ICHG_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    ICHG_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    IBUCK_20Hz = init_sos_filter(LPF_O2_F20, NULL);
    IBUCK_4Hz  = init_sos_filter(LPF_O2_F04, NULL);

    core_temp_4Hz   = init_sos_filter(LPF_O2_F04, NULL);
    pstage_temp_4Hz = init_sos_filter(LPF_O2_F04, NULL);
    cell_temp_4Hz   = init_sos_filter(LPF_O2_F04, NULL);
    VDD_4Hz         = init_sos_filter(LPF_O2_F04, NULL);

    analog_init();

    const TickType_t xMaxBlockTime = pdMS_TO_TICKS(100);
    uint32_t         ulNotifiedValue;

    float vlsb_12bit = NAN;

    for (;;) {

        // Wait to be notified of an interrupt.
        if (xTaskNotifyWait(pdFALSE, UINT32_MAX, &ulNotifiedValue, xMaxBlockTime)) {

            if (ulNotifiedValue & DMA2_TCIF5) {
                ASSERT(adc1_raw_results.vrefint != 0);
                // PSSKY2 does not have a dedicated high-precision analog reference voltage. Instead, the regular 3V3A
                // analog supply voltage is used as reference for ADC and DAC. Because this voltage cannot be considered
                // stable, its value is extrapolated from the processor's internal voltage reference (see RM0440 Rev 7
                // Section 21.4.33)
                const float vref = sosfilt(vref_20Hz, 3.0f * VREFINT_CAL / adc1_raw_results.vrefint);
                vlsb_12bit       = vref / FULL_RANGE_12BIT;

                const float VCC = adc1_raw_results.vcc_adc * vlsb_12bit * VCC_SCALING_FACTOR;
                const float VDD = adc1_raw_results.vbat * vlsb_12bit * VDD_SCALING_FACTOR;
                // the cell current is positive during charging phase and negative during the discharging phase
                // a -1 is multiplies with IBAT to follow the convention
                const float IBAT =
                    -1 * ((adc1_raw_results.ibat_adc * vlsb_12bit - VREF_OFFSET_VOLTAGE) * IBAT_SCALING_FACTOR);
                const float ICHG  = adc1_raw_results.ichg_adc * vlsb_12bit * ICHG_SCALING_FACTOR;
                const float IBUCK = adc1_raw_results.ibuck_adc * vlsb_12bit * IBUCK_SCALING_FACTOR;

                sosfilt_update(VCC_20Hz, VCC);
                sosfilt_update(VCC_4Hz, VCC);

                sosfilt_update(VDD_4Hz, VDD);

                sosfilt_update(IBAT_20Hz, IBAT);
                sosfilt_update(IBAT_4Hz, IBAT);

                sosfilt_update(ICHG_20Hz, ICHG);
                sosfilt_update(ICHG_4Hz, ICHG);

                sosfilt_update(IBUCK_20Hz, IBUCK);
                sosfilt_update(IBUCK_4Hz, IBUCK);

                temperatures.stm32_core = sosfilt(core_temp_4Hz, measure_stm32_core_temperature());
                temperatures.cell       = sosfilt(cell_temp_4Hz, measure_ntc_temperature(adc1_raw_results.cell_temp));
            }

            if (ulNotifiedValue & DMA2_TCIF6) {

                const float V48 = adc2_raw_results.v48_adc * vlsb_12bit * V48_SCALING_FACTOR;

                const float VSKY = adc2_raw_results.vsky_adc * vlsb_12bit * VSKY_SCALING_FACTOR;
                const float ISKY = adc2_raw_results.isky_adc * vlsb_12bit * ISKY_SCALING_FACTOR;

                const float VBAT = adc2_raw_results.vbat_adc * vlsb_12bit * VBAT_SCALING_FACTOR;

                sosfilt_update(V48_4Hz, V48);
                sosfilt_update(V48_20Hz, V48);

                sosfilt_update(VSKY_20Hz, VSKY);
                sosfilt_update(VSKY_4Hz, VSKY);

                sosfilt_update(ISKY_20Hz, ISKY);
                sosfilt_update(ISKY_4Hz, ISKY);

                sosfilt_update(VBAT_20Hz, VBAT);
                sosfilt_update(VBAT_4Hz, VBAT);

                temperatures.pstage = sosfilt(pstage_temp_4Hz, measure_ntc_temperature(adc2_raw_results.pstage_temp));
            }
        }
    }
}

static void analog_init() {
    // references in this function refer to RM0440 Rev 7

    // Section 22.4.6 DAC Output Voltage
    const float    VREF    = 3.3f;
    const uint32_t DAC_DOR = VREF_OFFSET_VOLTAGE * 4096 / VREF;  // DAC_OUT = Vref * DAC_DOR/4096

    DAC1->MCR |= (0ul << DAC_MCR_MODE1_Pos);  // DAC1 Normal mode with Output buffer enabled
    DAC1->DHR12R1 =
        (DAC_DOR << DAC_DHR12R1_DACC1DHR_Pos);  // Update data holding register with the 12 bit right aligned value
    DAC1->CR |= DAC_CR_EN1;                     // Enable DAC1 Output Channel 1
    // Each DAC channel is enabled after the tWAKEUP startup time.
    // DS12589 Rev 6, page 133: In normal mode when DAC buffer ON (tWAKEUP = 7.5us max)
    while (!READ_BIT(DAC1->SR, DAC_SR_DAC1RDY)) {
        // wait until DAC interface is ready to accept data
    }
    // RM0440 Rev 6 Section 22.4.5 DAC Conversion
    // DAC_DOR is loaded with a DAC_DHRx contents and the Analog output voltage becomes available after a time
    // tSETTLING
    // DS12589 Rev 6, page 133: Normal mode when DAC buffer is ON (tSETTLING = 3us max)
    vTaskDelay(1);

    ADC12_COMMON->CCR =
        (0b11 << ADC_CCR_CKMODE_Pos)  // derive ADC clock from AHB clock divided by 4 (40 MHz) - for more
                                      // details read RM0440 Rev 7, page 602, Section 21.4.3 ADC Clocks
        | ADC_CCR_VREFEN              // enable internal voltage measurement
        | ADC_CCR_VSENSESEL           // enable core temperature sensor
        | ADC_CCR_VBATSEL;            // enable stm32 supply voltage monoitoring

    // ============================================= TIM6 =============================================

    TIM6->PSC = 160 - 1;                   // make TIM6 run at 1 MHz
    TIM6->ARR = 10000 - 1;                 // make TIM6 overflow with 100 Hz
    TIM6->CR2 = 0b010 << TIM_CR2_MMS_Pos;  // use update (overflow) event as trigger output

    // ============================================= ADC CONFIGURATION =============================================

    // 21.4.6 ADC Deep-power-down mode (DEEPPWD) and ADC voltage regulator (ADVREGEN)
    CLEAR_BIT(ADC1->CR, ADC_CR_DEEPPWD);  // exit deep-power-down mode
    SET_BIT(ADC1->CR, ADC_CR_ADVREGEN);   // enable ADC voltage regulator
    CLEAR_BIT(ADC2->CR, ADC_CR_DEEPPWD);  // exit deep-power-down mode
    SET_BIT(ADC2->CR, ADC_CR_ADVREGEN);   // enable ADC voltage regulator

    // DS13122 Rev 3, page 121: ADC voltage regulator start-up time (Max) = 20 µs
    vTaskDelay(1);

    // 21.4.8 Calibration (ADCAL, ADCALDIF, ADC_CALFACT)
    SET_BIT(ADC1->CR, ADC_CR_ADCAL);  // start calibration
    SET_BIT(ADC2->CR, ADC_CR_ADCAL);  // start calibration

    while (READ_BIT(ADC1->CR, ADC_CR_ADCAL) || READ_BIT(ADC2->CR, ADC_CR_ADCAL)) {
        // wait for calibration to finish
        vTaskDelay(1);
    }

    // The ADEN bit cannot be set when ADCAL is set and during 4 ADC clock cycles
    // after the ADCAL bit is cleared by hardware (end of the calibration).
    // 21.4.9 ADC on-off Control (ADEN, ADDIS, ADRDY)
    vTaskDelay(1);

    ADC1->ISR = ADC_ISR_ADRDY;       // Clear ADRDY in ADC_ISR by writing '1'
    SET_BIT(ADC1->CR, ADC_CR_ADEN);  // enable ADC
    ADC2->ISR = ADC_ISR_ADRDY;       // Clear ADRDY in ADC_ISR by writing '1'
    SET_BIT(ADC2->CR, ADC_CR_ADEN);  // enable ADC

    // ADC needs a stabilization time (according to DS13122 Rev 3, page 121, tSTAB = 1 conversion on cycle), wait until
    // ADRDY = 1 and then clear off the bit in the ADC_ISR register
    while (!READ_BIT(ADC1->ISR, ADC_ISR_ADRDY) || !READ_BIT(ADC2->ISR, ADC_ISR_ADRDY)) {
        // wait until both ADCs are ready
    }
    ADC1->ISR = ADC_ISR_ADRDY;  // Clear ADRDY in ADC_ISR by writing '1'
    ADC2->ISR = ADC_ISR_ADRDY;  // Clear ADRDY in ADC_ISR by writing '1'

    // 21.4.11 Channel selection (SQRx, JSQRx)
    ADC1->SQR1 = (7ul << ADC_SQR1_L_Pos)        // sequence has 8 entries
                 | (2ul << ADC_SQR1_SQ1_Pos)    // IBUCK_ADC @ ADC1_IN2 (PA1, Pin 6)
                 | (3ul << ADC_SQR1_SQ2_Pos)    // ICHG_ADC @ ADC1_IN3 (PA2, Pin 7)
                 | (4ul << ADC_SQR1_SQ3_Pos)    // IBAT_ADC @ ADC1_IN4 (PA3, Pin 8)
                 | (10ul << ADC_SQR1_SQ4_Pos);  // VCC_ADC @ ADC1_IN10 (PF0, Pin 2)
    ADC1->SQR2 = (15ul << ADC_SQR2_SQ5_Pos)     // CELL_TEMP @ ADC1_IN15 (PB0, Pin 13)
                 | (16ul << ADC_SQR2_SQ6_Pos)   // Core Temperature @ ADC1_IN16
                 | (17ul << ADC_SQR2_SQ7_Pos)   // VBAT @ ADC1_IN17
                 | (18ul << ADC_SQR2_SQ8_Pos);  // VREFINT @ ADC1_IN18
    ADC2->SQR1 = (4ul << ADC_SQR1_L_Pos)        // sequence has 5 entries
                 | (1ul << ADC_SQR1_SQ1_Pos)    // ISKY_ADC @ ADC12_IN1 (PA0, Pin 5)
                 | (3ul << ADC_SQR1_SQ2_Pos)    // V48_ADC @ ADC2_IN3 (PA6, Pin 11)
                 | (4ul << ADC_SQR1_SQ3_Pos)    // VSKY_ADC @ ADC2_IN4 (PA7, Pin 12)
                 | (10ul << ADC_SQR1_SQ4_Pos);  // PSTAGE_TEMP @ ADC2_IN10 (PF1, Pin 3)
    ADC2->SQR2 = (13ul << ADC_SQR2_SQ5_Pos);    // VBAT_ADC @ ADC2_IN13 (PA5, Pin 10)

    // 21.4.12 Channel-wise programmable sampling time (SMPR1, SMPR2)
    // 6ul == 247.5 clock cycles @ 40 MHz == 6 µs sampling time
    ADC1->SMPR1 = (6ul << ADC_SMPR1_SMP2_Pos) | (6ul << ADC_SMPR1_SMP3_Pos) | (6ul << ADC_SMPR1_SMP4_Pos);
    ADC1->SMPR2 = (6ul << ADC_SMPR2_SMP10_Pos) | (6ul << ADC_SMPR2_SMP15_Pos) | (6ul << ADC_SMPR2_SMP16_Pos) |
                  (6ul << ADC_SMPR2_SMP18_Pos);
    // DS13122 Rev 3. Section 5.3.24. VBAT Monitoring Characteristics page 150
    // Minimum Sampling Time for VBAT monitoring is 12us
    ADC1->SMPR2 |= (7ul << ADC_SMPR2_SMP17_Pos);
    ADC2->SMPR1 = (6ul << ADC_SMPR1_SMP1_Pos) | (6ul << ADC_SMPR1_SMP3_Pos) | (6ul << ADC_SMPR1_SMP4_Pos);
    ADC2->SMPR2 = (6ul << ADC_SMPR2_SMP10_Pos) | (6ul << ADC_SMPR2_SMP13_Pos);

    // 21.4.18 Conversion on external trigger and trigger polarity (EXTSEL, EXTEN)
    // 21.4.26 Data management
    ADC1->CFGR = ADC_CFGR_JQDIS                   // disable Injected Queue (this is the default)
                 | (1ul << ADC_CFGR_EXTEN_Pos)    // Hardware Trigger with detection on rising edge
                 | (13ul << ADC_CFGR_EXTSEL_Pos)  // TIM6_TRGO
                 | ADC_CFGR_DMAEN                 // generate DMA requests
                 | ADC_CFGR_DMACFG;               // DMA operates in circular mode

    DMA2_Channel5->CCR = (0ul << DMA_CCR_PL_Pos)       // low priority
                         | (1ul << DMA_CCR_MSIZE_Pos)  // 16-bit
                         | (1ul << DMA_CCR_PSIZE_Pos)  // 16-bit
                         | DMA_CCR_MINC                // memory increment mode
                         | DMA_CCR_CIRC                // circular mode
                         | DMA_CCR_TCIE;               // transfer complete interrupt enable
    DMA2_Channel5->CPAR  = (uint32_t) &ADC1->DR;
    DMA2_Channel5->CMAR  = (uint32_t) &adc1_raw_results;
    DMA2_Channel5->CNDTR = ((ADC1->SQR1 & ADC_SQR1_L_Msk) >> ADC_SQR1_L_Pos) + 1;
    // DMAMUX Channel 10 is hardwired to DMA2 Channel 5
    DMAMUX1_Channel10->CCR = (5ul << DMAMUX_CxCR_DMAREQ_ID_Pos);  // 5: ADC1

    ADC2->CFGR = ADC_CFGR_JQDIS                   // disable Injected Queue (this is the default)
                 | (1ul << ADC_CFGR_EXTEN_Pos)    // Hardware Trigger with detection on rising edge
                 | (13ul << ADC_CFGR_EXTSEL_Pos)  // TIM6_TRGO
                 | ADC_CFGR_DMAEN                 // generate DMA requests
                 | ADC_CFGR_DMACFG;               // DMA operates in circular mode

    DMA2_Channel6->CCR = (0ul << DMA_CCR_PL_Pos)       // low priority
                         | (1ul << DMA_CCR_MSIZE_Pos)  // 16-bit
                         | (1ul << DMA_CCR_PSIZE_Pos)  // 16-bit
                         | DMA_CCR_MINC                // memory increment mode
                         | DMA_CCR_CIRC                // circular mode
                         | DMA_CCR_TCIE;               // transfer complete interrupt enable
    DMA2_Channel6->CPAR  = (uint32_t) &ADC2->DR;
    DMA2_Channel6->CMAR  = (uint32_t) &adc2_raw_results;
    DMA2_Channel6->CNDTR = ((ADC2->SQR1 & ADC_SQR1_L_Msk) >> ADC_SQR1_L_Pos) + 1;
    // DMAMUX Channel 11 is hardwired to DMA2 Channel 6
    DMAMUX1_Channel11->CCR = (36ul << DMAMUX_CxCR_DMAREQ_ID_Pos);  // 36: ADC2

    // Register and enable ADC1 DMA2 Channel 5 interrupt
    NVIC_SetPriority(DMA2_Channel5_IRQn, InterruptPriorityDMA2Channel5);
    NVIC_EnableIRQ(DMA2_Channel5_IRQn);
    PERIPH_BIT(DMA2_Channel5->CCR, DMA_CCR_EN_Pos) = 1;  // enable DMA2 Channel 5

    // Register and enable ADC2 DMA2 Channel 6 interrupt
    NVIC_SetPriority(DMA2_Channel6_IRQn, InterruptPriorityDMA2Channel6);
    NVIC_EnableIRQ(DMA2_Channel6_IRQn);
    PERIPH_BIT(DMA2_Channel6->CCR, DMA_CCR_EN_Pos) = 1;  // enable DMA2 Channel 6

    // 21.4.15 Starting conversions (ADSTART)
    PERIPH_BIT(TIM6->CR1, TIM_CR1_CEN_Pos) = 1;  // start TIM6
    SET_BIT(ADC1->CR, ADC_CR_ADSTART);           // start ADC1 regular conversions, triggered by TIM6
    SET_BIT(ADC2->CR, ADC_CR_ADSTART);           // start ADC2 regular conversions, triggered by TIM6

    LOG_INFO("Analog subsystem initialized.");
}

void DMA2_Channel5_IRQHandler(void) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (DMA2->ISR & DMA_ISR_TCIF5) {
        xTaskNotifyFromISR(xHandlingTask, DMA2_TCIF5, eSetBits, &xHigherPriorityTaskWoken);
        DMA2->IFCR = DMA_IFCR_CTCIF5;  // clear the transfer complete flag for DMA2 Channel 5
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void DMA2_Channel6_IRQHandler(void) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (DMA2->ISR & DMA_ISR_TCIF6) {
        xTaskNotifyFromISR(xHandlingTask, DMA2_TCIF6, eSetBits, &xHigherPriorityTaskWoken);
        DMA2->IFCR = DMA_IFCR_CTCIF6;  // clear the transfer complete flag for DMA2 Channel 6
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static float measure_stm32_core_temperature(void) {
    const float TS_DATA = adc1_raw_results.stm32_core * get_external_reference_voltage() / (1e-3f * TS_CAL_VREF_mV);
    // RM0440 Rev 7, page 678, section 21.4.31 Temperature sensor: Reading the temperature
    const float core_temp_celsius =
        (float) (TS_CAL2_TEMP - TS_CAL1_TEMP) / (float) (TS_CAL2 - TS_CAL1) * (TS_DATA - TS_CAL1) + 30;
    return core_temp_celsius + T0;  // in kelvin
}

float measure_ntc_temperature(uint16_t adc_value) {
    const bool  valid = 16 <= adc_value && adc_value < 4080;
    const float R1    = 4.7e3f;
    const float R25   = 10e3f;
    const float B2585 = 3434.0f;
    return valid ? calculate_temperature_from_NTC(R1, R25, adc_value, FULL_RANGE_12BIT + 1, B2585) : NAN;
}

static float calculate_temperature_from_NTC(float R1_25, float RNTC_25, float VT, float Vref, float B2585) {
    return 1 / (logf((R1_25 / RNTC_25) * (VT / (Vref - VT))) / B2585 + 1 / T25);
}
