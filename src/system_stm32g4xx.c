/**
 ******************************************************************************
 * @file    system_stm32g4xx.c
 * @author  MCD Application Team
 * @brief   CMSIS Cortex-M4 Device Peripheral Access Layer System Source File
 *
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <device.h>

#if !defined(HSE_VALUE)
#    define HSE_VALUE 24000000U /*!< Value of the External oscillator in Hz */
#endif                          /* HSE_VALUE */

#if !defined(HSI_VALUE)
#    define HSI_VALUE 16000000U /*!< Value of the Internal oscillator in Hz*/
#endif                          /* HSI_VALUE */

uint32_t SystemCoreClock = HSI_VALUE;

const uint8_t AHBPrescTable[16] = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U, 6U, 7U, 8U, 9U};
const uint8_t APBPrescTable[8]  = {0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U};

void set_up_power_mode(void) {
    // Switching from Range1 normal mode to Range1 boost mode as in RM0440, Section 6.1.5:
    //
    // 1. The system clock must be divided by 2 using the AHB prescaler before switching to a higher system frequency.
    // 2. Clear the R1MODE bit is in the PWR_CR5 register.
    // 3. Adjust the number of wait states according to the new frequency target in range1 boost mode
    // 4. Configure and switch to new system frequency.
    // 5. Wait for at least 1us and then reconfigure the AHB prescaler to get the needed HCLK clock frequency.

    PERIPH_BIT(PWR->CR5, PWR_CR5_R1MODE_Pos) = 0;
}

void set_up_oscillator(void) {
    PERIPH_BIT(RCC->CR, RCC_CR_HSION_Pos)         = 1;  // enable HSI
    PERIPH_BIT(RCC->CRRCR, RCC_CRRCR_HSI48ON_Pos) = 1;  // enable HSI48

    // PLL Configuration Change as in RM0440, Section 7.2.4 PLL
    //
    // 1. Disable the PLL by setting PLLON to 0 in Clock control register (RCC_CR).
    // 2. Wait until PLLRDY is cleared. The PLL is now fully stopped.
    // 3. Change the desired parameter.
    // 4. Enable the PLL again by setting PLLON to 1.
    // 5. Enable the desired PLL outputs by configuring PLLPEN, PLLQEN, PLLREN in RCC_PLLCFGR.
    PERIPH_BIT(RCC->CR, RCC_CR_PLLON_Pos) = 0;
    while (PERIPH_BIT(RCC->CR, RCC_CR_PLLRDY_Pos) != 0) {
        // wait until PLLRDY is cleared
    }

    RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC_HSI            // feed PLL from HSI16             →   16 MHz
                 | (0b11ul << RCC_PLLCFGR_PLLM_Pos)  // divide by four                  →    4 MHz
                 | (80ul << RCC_PLLCFGR_PLLN_Pos)    // multiply with 80                →  320 MHz
                 | (0b00ul << RCC_PLLCFGR_PLLQ_Pos)  // PLL "Q" output: divide by two   →  160 MHz
                 | (0b00ul << RCC_PLLCFGR_PLLR_Pos)  // PLL "R" output: divide by two   →  160 MHz
        ;

    PERIPH_BIT(RCC->CR, RCC_CR_PLLON_Pos) = 1;
    while (PERIPH_BIT(RCC->CR, RCC_CR_PLLRDY_Pos) == 0) {
        // wait until PLLRDY is set
    }

    PERIPH_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLLQEN_Pos) = 1;  // enable PLL "Q" output
    PERIPH_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLLREN_Pos) = 1;  // enable PLL "R" output

    while (PERIPH_BIT(RCC->CRRCR, RCC_CRRCR_HSI48RDY_Pos) == 0) {
        // wait until HSI48 reports ready state
    }
}

void set_up_clocks(void) {
    // Increasing the CPU freuqency as in RM0440, Section 5.3.3 Read access latency
    //
    // 1. Program the new number of wait states to the LATENCY bits in the Flash access control register (FLASH_ACR).
    // 2. Check that the new number of wait states is taken into account to access the Flash memory by reading the
    //    FLASH_ACR register.
    // 3. Analyze the change of CPU frequency change caused either by:
    //    – changing clock source defined by SW bits in RCC_CFGR register
    //    – or by CPU clock prescaller defined by HPRE bits in RCC_CFGR
    //    If some of above two steps decreases the CPU frequency, firstly perform this step and then the rest. Otherwise
    //    modify The CPU clock source by writing the SW bits in the RCC_CFGR register and then (if needed) modify the
    //    CPU clock prescaler by writing the HPRE bits in RCC_CFGR.
    // 4. Check that the new CPU clock source or/and the new CPU clock prescaler value is/are taken into account by
    //    reading the clock source status (SWS bits) or/and the AHB prescaler value (HPRE bits), respectively, in the
    //    RCC_CFGR register.
    FLASH->ACR = (FLASH->ACR & ~FLASH_ACR_LATENCY_Msk) | FLASH_ACR_LATENCY_4WS;  // 160 MHz at Range 1 boost mode
    while ((FLASH->ACR & FLASH_ACR_LATENCY_Msk) != FLASH_ACR_LATENCY_4WS) {
        // we've got a problem
    }

    // divide HCLK by two
    RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_HPRE_Msk) | RCC_CFGR_HPRE_DIV2;
    while ((RCC->CFGR & RCC_CFGR_HPRE_Msk) != RCC_CFGR_HPRE_DIV2) {
        // wait for the switch to complete
    }

    // switch to PLL
    RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_SW_Msk) | RCC_CFGR_SW_PLL;
    while ((RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_PLL) {
        // wait for the switch to complete
    }

    for (uint32_t delay = 0; delay < 100; delay++) {
        __NOP();
    }

    // reset HCLK prescaler
    RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_HPRE_Msk) | RCC_CFGR_HPRE_DIV1;
    while ((RCC->CFGR & RCC_CFGR_HPRE_Msk) != RCC_CFGR_HPRE_DIV1) {
        // wait for the switch to complete
    }

    SystemCoreClockUpdate();
}

void SystemInit(void) {
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
    // Cortex™-M4 Devices Generic User Guide, Section 4.6.1 Coprocessor Access Control Register
    SCB->CPACR |= ((3UL << (10 * 2)) | (3UL << (11 * 2)));  // set CP10 and CP11 Full Access
#endif

    /* ======== Configure NVIC Priority Grouping ======== */

    // Cortex™-M4 Devices Generic User Guide, Table 4-18 Priority grouping
    NVIC_SetPriorityGrouping(0u);

    /* ======== Enable First Peripheral Clocks ======== */

    PERIPH_BIT(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN_Pos) = 1;
    (void) PERIPH_BIT(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN_Pos);

    PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_PWREN_Pos) = 1;
    (void) PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_PWREN_Pos);

    PERIPH_BIT(PWR->CR3, PWR_CR3_UCPD_DBDIS_Pos) = 1;  // Disable USB Type-C dead battery pull-down behavior

    /* ======== Configure Power Supply ======== */

    set_up_power_mode();

    /* ======== Set up Oscillator ======== */

    set_up_oscillator();

    /* ======== Set up System and Bus Clocks ======== */

    set_up_clocks();
}

/**
 * @brief  Update SystemCoreClock variable according to Clock Register Values.
 *         The SystemCoreClock variable contains the core clock (HCLK), it can
 *         be used by the user application to setup the SysTick timer or configure
 *         other parameters.
 *
 * @note   Each time the core clock (HCLK) changes, this function must be called
 *         to update SystemCoreClock variable value. Otherwise, any configuration
 *         based on this variable will be incorrect.
 *
 * @note   - The system frequency computed by this function is not the real
 *           frequency in the chip. It is calculated based on the predefined
 *           constant and the selected clock source:
 *
 *           - If SYSCLK source is HSI, SystemCoreClock will contain the HSI_VALUE(**)
 *
 *           - If SYSCLK source is HSE, SystemCoreClock will contain the HSE_VALUE(***)
 *
 *           - If SYSCLK source is PLL, SystemCoreClock will contain the HSE_VALUE(***)
 *             or HSI_VALUE(*) multiplied/divided by the PLL factors.
 *
 *         (**) HSI_VALUE is a constant defined in stm32g4xx_hal.h file (default value
 *              16 MHz) but the real value may vary depending on the variations
 *              in voltage and temperature.
 *
 *         (***) HSE_VALUE is a constant defined in stm32g4xx_hal.h file (default value
 *              24 MHz), user has to ensure that HSE_VALUE is same as the real
 *              frequency of the crystal used. Otherwise, this function may
 *              have wrong result.
 *
 *         - The result of this function could be not correct when using fractional
 *           value for HSE crystal.
 *
 * @param  None
 * @retval None
 */
void SystemCoreClockUpdate(void) {
    uint32_t tmp, pllvco, pllr, pllsource, pllm;

    /* Get SYSCLK source -------------------------------------------------------*/
    switch (RCC->CFGR & RCC_CFGR_SWS) {
        case 0x04: /* HSI used as system clock source */
            SystemCoreClock = HSI_VALUE;
            break;

        case 0x08: /* HSE used as system clock source */
            SystemCoreClock = HSE_VALUE;
            break;

        case 0x0C: /* PLL used as system clock  source */
            /* PLL_VCO = (HSE_VALUE or HSI_VALUE / PLLM) * PLLN
               SYSCLK = PLL_VCO / PLLR
               */
            pllsource = (RCC->PLLCFGR & RCC_PLLCFGR_PLLSRC);
            pllm      = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLM) >> 4) + 1U;
            if (pllsource == 0x02UL) /* HSI used as PLL clock source */ {
                pllvco = (HSI_VALUE / pllm);
            } else /* HSE used as PLL clock source */ {
                pllvco = (HSE_VALUE / pllm);
            }
            pllvco          = pllvco * ((RCC->PLLCFGR & RCC_PLLCFGR_PLLN) >> 8);
            pllr            = (((RCC->PLLCFGR & RCC_PLLCFGR_PLLR) >> 25) + 1U) * 2U;
            SystemCoreClock = pllvco / pllr;
            break;

        default:
            break;
    }
    /* Compute HCLK clock frequency --------------------------------------------*/
    /* Get HCLK prescaler */
    tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];
    /* HCLK clock frequency */
    SystemCoreClock >>= tmp;
}
