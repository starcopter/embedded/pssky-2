/**
 * @file main.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Main Task Implementation
 * @version 0.1
 * @date 2021-12-03
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 */

#include <main.h>
#include "apps.h"

#include <sclib/assert.h>
#include <sclib/console.h>
#include <canard/canard.h>
#include <canard/fdcan.h>
#include <canard/node.h>
#include <sclib/registry.h>
#include <sclib/timekeeper.h>

#include <uavcan/node/Heartbeat_1_0.h>
#include <reg/udral/physics/electricity/SourceTs_0_1.h>
#include <starcopter/aeric/pssky2/SystemStatus_0_1.h>
#include <starcopter/aeric/pssky2/PSSKY2Status_0_2.h>
#include <starcopter/aeric/pssky2/Temperatures_0_1.h>

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>
#include <timers.h>

static inline void enable_charger() {
    GPIOA->BSRR = GPIO_BSRR_BS15;  // set CHG_TS pin HIGH to enable charging
}

static inline void disable_charger() {
    GPIOA->BSRR = GPIO_BSRR_BR15;  // set CHG_TS pin LOW to disable charging
}

SemaphoreHandle_t pulse_detected;

static TimerHandle_t led_fade_out, strobe_timer_handle, charger_enable_timer_handle;
static void          strobe_timer_callback(TimerHandle_t handle);
static void          led_fade_out_callback(TimerHandle_t handle);
static void          charger_enable_callback(TimerHandle_t handle);

const TickType_t STROBE_IDLE_DURATION         = pdMS_TO_TICKS(4000);
const TickType_t LED_ON_DURATION              = pdMS_TO_TICKS(50);
const TickType_t DELAY_BEFORE_CHARGER_ENABLED = pdMS_TO_TICKS(1000);

static volatile bool charger_state;

static void Main(void* param);

/**
 * @brief Print Platform Specific Information
 * @details This function prints GNU build ID, git hash code (20 byte), hardware version number, boot ID, and device
 * Unique identifier (128 bit identifier).
 */
void print_platform_info(void);

/**
 * @brief Initialize RTOS Timers
 * @details This function initializes freeRTOS timers periodically to trigger publish DC_bus, pssky2 source, pssky2
 * status, system status and temperatures over the Cyphal Bus.
 */
static void init_timers(void);

/**
 * @brief Publish DC Bus message
 * @details This function populate the DC Bus data (48V net), serialize data, set canard meta data and call node publish
 * API.
 * @note The publication rate is set to 20Hz
 * @param __unused no arguments are being sent
 */
static void publish_DC_bus_message(TimerHandle_t source_timer __unused);

/**
 * @brief Publish PSSKY2 Source Message
 * @details This function populate the PSSKY2 Source data (VSKY, ISKY), serialize data, set canard meta data and call
 * node publish API.
 * @note The publication rate is set to 10Hz
 * @param __unused no arguments are being sent
 */
static void publish_pssky2_source_message(TimerHandle_t pssky2_source_timer __unused);

/**
 * @brief Publish PSSKY2 Status Message
 * @details This function populate the PSSKY2 Status data (PSSKY2 ON, Power Good), serialize data, set canard meta data
 * and call node publish API.
 * @note The publication rate is set to 10Hz
 * @param __unused no arguments are being sent
 */
static void publish_pssky2_status_message(TimerHandle_t pssky2_status_timer __unused);

/**
 * @brief Publish PSSKY2 System Status message
 * @details This function populate the PSSKY2 System Status (cell voltage & current, cell charge current, buck current,
 * boost voltage, stm32 internal reference and supply voltage), serialize data, set canard meta data and call node
 * publish API.
 * @note The publication rate is set to 6.7Hz
 * @param __unused no arguments are being sent
 */
static void publish_system_status_message(TimerHandle_t system_status_timer __unused);

/**
 * @brief Publish PSSKY2 Temperature message
 * @details This function populate the PSSKY2 On board Temperature measurements (cell temperature, stm32 core, power
 * stage), serialize data, set canard meta data and call node publish API.
 * @note The publication rate is set to 5Hz
 * @param __unused no arguments are being sent
 */
static void publish_temperature_message(TimerHandle_t temperature_timer __unused);

struct PSSKY2Status {
    uint8_t pssky2_on   : 1;  // 5V Skynode Secondary: voltage enable
    uint8_t pssky2_pg   : 1;  // 5V Skynode Secondary: power good
    uint8_t charge_stat : 2;  // cell charger status
};
static struct __PACKED PSSKY2Status ps_status;
static struct __PACKED PSSKY2Status _last_published_ps_status;

void start_main_task(void) {
    static TaskHandle_t handle = NULL;
    ASSERT(handle == NULL);

    BaseType_t status = xTaskCreate(Main, "Main", 512, NULL, TaskPrioritySlow, &handle);
    ASSERT(status == pdPASS);
}

static void Main(void* param __unused) {

    {
        const UAVCANRegister_t* const reg = get_register_by_name("uavcan.node.id");
        ASSERT(reg);
        uint16_t* nid = (uint16_t*) reg->value;
        if (*nid > CANARD_NODE_ID_MAX) {
            *nid = UID->WORD1 % 79 + 20;  // deterministic pseudo-random number in [20, 99]
        }
    }

    node_init();
    print_platform_info();
    start_node_monitor_task();
    start_analog_task();
    init_ina2xx_emulation();

    // TODO: perform redundancy check on Skynode power supplies
    // https://gitlab.com/starcopter/embedded/motherboard/-/issues/7
    lock_pssky2_enable_pins();

    init_timers();

    pulse_detected = xSemaphoreCreateBinary();
    ASSERT(pulse_detected);

    NVIC_SetPriority(EXTI4_IRQn, InterruptPriorityPulseDetected);
    NVIC_EnableIRQ(EXTI4_IRQn);

    int count = 0;
    charger_state = 0; // charger disabled by default

    while (1) {

        // TO DO! A proper PSSKY2 PG needs to be implemented, The below is just a weak fix
        ps_status.pssky2_pg = READ_BIT(GPIOB->IDR, GPIO_IDR_ID5) ? 0 : 1;  // PB5: CHG_PG
        // To DO! A proper VSKY2 EN needs to be implemented, the below is just a weal fix
        ps_status.pssky2_on = READ_BIT(GPIOA->IDR, GPIO_IDR_ID10) ? 1 : 0;  // PA10: VSKY_EN

        // wait for more than a second to see if the pulse is detected
        if (xSemaphoreTake(pulse_detected, pdMS_TO_TICKS(1200)) == pdPASS) {
            count += 1;  // update pulse counter
            // if 3 consecutive pulses are detected, then its a fault state
            if (count >= 3) {
                ps_status.charge_stat = FAULT;
                reset_led(LED_GREEN);
                set_led(LED_RED);
            }
        } else {
            reset_led(LED_RED);
            ps_status.charge_stat = (READ_BIT(GPIOB->IDR, GPIO_IDR_ID4) ? 1 : 0) ? DISABLED : CHARGING;
            if (ps_status.charge_stat == CHARGING) {
                set_led(LED_GREEN);
            } else {
                reset_led(LED_GREEN);
            }
            count = 0;
        }

        // check if the v48 supply is greater than 5V (buck converter works between 4.5V to 60V)
        const bool v48_supply_connected = (get_system_voltage() > 5) ? 1 : 0;
        if (!charger_state && v48_supply_connected && !xTimerIsTimerActive(charger_enable_timer_handle)) {
            xTimerStart(charger_enable_timer_handle, portMAX_DELAY);
        } else if (!v48_supply_connected && !xTimerIsTimerActive(strobe_timer_handle)) {
            disable_charger();
            charger_state = 0;
            // if the cell is not charging, start timer that strobes the led when the timer expires
            xTimerStart(strobe_timer_handle, portMAX_DELAY);
        }
    }
}

static void charger_enable_callback(TimerHandle_t handle) {
    // enable charger once the timer expired
    enable_charger();
    charger_state = 1;
}

static void strobe_timer_callback(TimerHandle_t handle) {
    // check once more if the v48 supply is present or not
    const bool v48_supply_connected = (get_system_voltage() > 1) ? 1 : 0;
    if (!v48_supply_connected) {
        xTimerStart(led_fade_out, portMAX_DELAY);
        set_led(LED_RED);
    }
}

static void led_fade_out_callback(TimerHandle_t handle) {
    reset_led(LED_RED);
}

void EXTI4_IRQHandler(void) {

    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (EXTI->PR1 & EXTI_PR1_PIF4) {  // if the PB4 trigger the interrupt
        xSemaphoreGiveFromISR(pulse_detected, &xHigherPriorityTaskWoken);
        EXTI->PR1 |= (1ul << EXTI_PR1_PIF4_Pos);  // clear the interrupt flag by writing a 1 to the pending register
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void init_timers() {
    // Create an RTOS timer and periodically publish DC Bus messages every after 50ms (20Hz publication rate)
    TimerHandle_t source_timer = xTimerCreate("DC_bus", pdMS_TO_TICKS(50), pdTRUE, NULL, publish_DC_bus_message);
    xTimerStart(source_timer, portMAX_DELAY);

    // Create an RTOS timer and periodically publish PSSKY2 Source messages every after 100ms (10Hz publication rate)
    TimerHandle_t pssky2_source_timer =
        xTimerCreate("PSSKY2_Source", pdMS_TO_TICKS(100), pdTRUE, NULL, publish_pssky2_source_message);
    xTimerStart(pssky2_source_timer, portMAX_DELAY);

    // Create an RTOS timer and periodically publish PSSKY2 Status messages every after 1s (1Hz publication rate)
    TimerHandle_t pssky2_status_timer =
        xTimerCreate("PSSKY2_Status", pdMS_TO_TICKS(1000), pdTRUE, NULL, publish_pssky2_status_message);
    xTimerStart(pssky2_status_timer, portMAX_DELAY);

    // create an RTOS timer and periodically publish PSSKY2 System Status messages every after 150ms (6.7Hz publication
    // rate)
    TimerHandle_t system_status_timer =
        xTimerCreate("System_Status", pdMS_TO_TICKS(150), pdTRUE, NULL, publish_system_status_message);
    xTimerStart(system_status_timer, portMAX_DELAY);

    // Create an RTOS timer and periodically publish temperature messages every after 200ms (5Hz publication rate)
    TimerHandle_t temperature_timer =
        xTimerCreate("Temperatures", pdMS_TO_TICKS(200), pdTRUE, NULL, publish_temperature_message);
    xTimerStart(temperature_timer, portMAX_DELAY);

    strobe_timer_handle = xTimerCreate("Display Timer", STROBE_IDLE_DURATION, pdFALSE, NULL, strobe_timer_callback);
    ASSERT(strobe_timer_handle != NULL);

    led_fade_out = xTimerCreate("LED Fade Out", LED_ON_DURATION, pdFALSE, NULL, led_fade_out_callback);
    ASSERT(led_fade_out != NULL);

    charger_enable_timer_handle =
        xTimerCreate("Charger Enable Timer", DELAY_BEFORE_CHARGER_ENABLED, pdFALSE, NULL, charger_enable_callback);
    ASSERT(charger_enable_timer_handle != NULL);
}

void print_platform_info(void) {
    const char* const name = image_hdr.name;
    char              image_version_string[VERSION_STRING_MAXSZ];
    image_render_version_string(image_version_string, &image_hdr.version, image_hdr.clean_build);
    const HWVersion_t* const hw = &device_configuration_block.release;
    LOG_NOTICE("%s %s on HW %c.%u%s", name, image_version_string, hw->major, hw->minor, dcb_validate() ? "" : " (?)");

    extern const ELFNote_t g_note_build_id;
    LOG_INFO("Git Rev %08lx%s, Build ID %08lx, CRC %08lx/%08lx", __builtin_bswap32(*(uint32_t*) &image_hdr.git_sha[0]),
             image_hdr.clean_build ? "" : "*",
             __builtin_bswap32(*(uint32_t*) &g_note_build_id.data[g_note_build_id.namesz]), image_hdr.header_crc,
             __image_crc);
    LOG_INFO("Running on device %08lx", __builtin_bswap32(*(uint32_t*) &device_configuration_block.uid[12]));

    extern uint32_t boot_id[4];
    LOG_DEBUG("Boot ID %08lx%08lx%08lx%08lx", boot_id[3], boot_id[2], boot_id[1], boot_id[0]);
}

// =========================== REGISTER IMPLEMENTATION ===========================

// uavcan.pub.dc_bus: reg.udral.physics.electricity.SourceTs.0.1 @ 20 Hz
volatile uint16_t dc_bus_subject_id     = SUBJECT_ID_DEFAULT;
static const char DC_BUS_SUBJECT_TYPE[] = reg_udral_physics_electricity_SourceTs_0_1_FULL_NAME_AND_VERSION_;

static void publish_DC_bus_message(TimerHandle_t source_timer __unused) {
    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = dc_bus_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    const reg_udral_physics_electricity_SourceTs_0_1 obj = {
        .timestamp = {get_synchronized_timestamp_us()},
        .value     = {.power       = {.voltage = {get_system_voltage()}, .current = {NAN}},
                      .energy      = {NAN},
                      .full_energy = {NAN}},
    };

    size_t   size = reg_udral_physics_electricity_SourceTs_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);

    const int_fast8_t result = reg_udral_physics_electricity_SourceTs_0_1_serialize_(&obj, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);

    const CanardTransferMetadata meta = {.priority       = CanardPriorityLow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(1000ul, &meta, size, buf);
    vPortFree(buf);
}

// uavcan.pub.dc_bus: reg.udral.physics.electricity.SourceTs.0.1 @ 10 Hz
volatile uint16_t pssky2_source_subject_id     = SUBJECT_ID_DEFAULT;
static const char PSSKY2_SOURCE_SUBJECT_TYPE[] = reg_udral_physics_electricity_SourceTs_0_1_FULL_NAME_AND_VERSION_;

static void publish_pssky2_source_message(TimerHandle_t pssky2_source_timer __unused) {
    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = pssky2_source_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    const reg_udral_physics_electricity_SourceTs_0_1 obj = {
        .timestamp = {get_synchronized_timestamp_us()},
        .value     = {.power       = {.voltage = {get_skynode_secondary_supply_voltage()},
                                      .current = {get_skynode_secondary_supply_current()}},
                      .energy      = {NAN},
                      .full_energy = {NAN}},
    };

    size_t   size = reg_udral_physics_electricity_SourceTs_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);

    const int_fast8_t result = reg_udral_physics_electricity_SourceTs_0_1_serialize_(&obj, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);

    const CanardTransferMetadata meta = {.priority       = CanardPriorityLow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(1000ul, &meta, size, buf);
    vPortFree(buf);
}

// uavcan.pub.pssky2_status: starcopter.aeric.pssky2.PSSKY2Status.0.2.dsdl @ 10 Hz
volatile uint16_t pssky2_status_subject_id     = SUBJECT_ID_DEFAULT;
static const char PSSKY2_STATUS_SUBJECT_TYPE[] = starcopter_aeric_pssky2_PSSKY2Status_0_2_FULL_NAME_AND_VERSION_;

static void publish_pssky2_status_message(TimerHandle_t pssky2_status_timer __unused) {
    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = pssky2_status_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    // manual "serialized" representation of a starcopter.aeric.pssky2.PSSKY2Status.0.2 message
    const struct __PACKED PSSKY2Status01 {
        uint64_t            timestamp;
        struct PSSKY2Status pstat;
    } msg = {.timestamp = get_synchronized_timestamp_us(), .pstat = ps_status};
    static_assert(sizeof(msg) == starcopter_aeric_pssky2_PSSKY2Status_0_2_SERIALIZATION_BUFFER_SIZE_BYTES_,
                  "Something's wrong");

    const CanardTransferMetadata meta = {.priority       = CanardPrioritySlow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(100000ul, &meta, sizeof(msg), (uint8_t*) &msg);
    _last_published_ps_status = ps_status;
}

// uavcan.pub.system_status: com.starcopter.aeric.pssky2.SystemsStatus.0.1 @ 6.7 Hz
volatile uint16_t system_status_subject_id     = SUBJECT_ID_DEFAULT;
static const char SYSTEM_STATUS_SUBJECT_TYPE[] = starcopter_aeric_pssky2_SystemStatus_0_1_FULL_NAME_AND_VERSION_;

static void publish_system_status_message(TimerHandle_t system_status_timer __unused) {
    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = system_status_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    const starcopter_aeric_pssky2_SystemStatus_0_1 obj = {
        .timestamp           = {get_synchronized_timestamp_us()},
        .cell_voltage        = {get_cell_voltage()},
        .cell_current        = {get_cell_current()},
        .cell_charge_current = {get_cell_charge_current()},
        .boost_stage_voltage = {get_boost_stage_output_voltage()},
        .buck_stage_current  = {get_buck_stage_output_current()},
        .vrefint             = {get_external_reference_voltage()},
        .vbat                = {get_stm32_supply_voltage()},
    };

    size_t   size = starcopter_aeric_pssky2_SystemStatus_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);

    const int_fast8_t result = starcopter_aeric_pssky2_SystemStatus_0_1_serialize_(&obj, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);

    const CanardTransferMetadata meta = {.priority       = CanardPriorityLow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(1000ul, &meta, size, buf);
    vPortFree(buf);
}

// uavcan.pub.temperatures: starcopter.aeric.pssky2.Temperatures.0.1 @5Hz
volatile uint16_t temperature_subject_id     = SUBJECT_ID_DEFAULT;
static const char temperature_subject_type[] = starcopter_aeric_pssky2_Temperatures_0_1_FULL_NAME_AND_VERSION_;

static void publish_temperature_message(TimerHandle_t temperature_timer __unused) {

    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = temperature_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    const struct Temperatures* temperatures = get_temperatures();

    // manual "serialized" representation of a starcopter.aeric.pssky2.Temperatures.0.1 message, protobuf-style
    const struct __PACKED Temperatures01 {
        uint64_t            timestamp;
        struct Temperatures temperatures;
    } msg = {
        .timestamp    = get_synchronized_timestamp_us(),
        .temperatures = *temperatures,
    };
    static_assert(sizeof(msg) == starcopter_aeric_pssky2_Temperatures_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_,
                  "Something's wrong");

    const CanardTransferMetadata meta = {.priority       = CanardPriorityLow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(100000ul, &meta, sizeof(msg), (uint8_t*) &msg);
}

// =========================== REGISTER DECLARATION ===========================

// uavcan.pub.DC_bus.id
UAVCANRegisterDeclaration _reg_pub_DC_bus_id = {
    .name        = "uavcan.pub.DC_bus.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &dc_bus_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.DC_bus.type
UAVCANRegisterDeclaration _reg_pub_DC_bus_type = {
    .name        = "uavcan.pub.DC_bus.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(DC_BUS_SUBJECT_TYPE) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) DC_BUS_SUBJECT_TYPE,
    .update_hook = NULL,
    .lock        = NULL,
};

// uavcan.pub.pssky2_source.id
UAVCANRegisterDeclaration _reg_pssky2_source_bus_id = {
    .name        = "uavcan.pub.pssky2_source.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &pssky2_source_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.pssky2_source.type
UAVCANRegisterDeclaration _reg_pssky2_source_bus_type = {
    .name        = "uavcan.pub.pssky2_source.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(PSSKY2_SOURCE_SUBJECT_TYPE) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) PSSKY2_SOURCE_SUBJECT_TYPE,
    .update_hook = NULL,
    .lock        = NULL,
};

// uavcan.pub.pssky2_status.id
UAVCANRegisterDeclaration _reg_pub_pssky2_status_id = {
    .name        = "uavcan.pub.pssky2_status.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &pssky2_status_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.pssky2_status.type
UAVCANRegisterDeclaration _reg_pub_pssky2_status_type = {
    .name        = "uavcan.pub.pssky2_status.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(PSSKY2_STATUS_SUBJECT_TYPE) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) PSSKY2_STATUS_SUBJECT_TYPE,
    .update_hook = NULL,
    .lock        = NULL,
};

// uavcan.pub.system_status.id
UAVCANRegisterDeclaration _reg_system_status_bus_id = {
    .name        = "uavcan.pub.system_status.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &system_status_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.system_status.type
UAVCANRegisterDeclaration _reg_system_status_bus_type = {
    .name        = "uavcan.pub.system_status.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(SYSTEM_STATUS_SUBJECT_TYPE) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) SYSTEM_STATUS_SUBJECT_TYPE,
    .update_hook = NULL,
    .lock        = NULL,
};

// uavcan.pub.temperatures.id
UAVCANRegisterDeclaration _reg_pub_temperatures_id = {
    .name        = "uavcan.pub.temperatures.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &temperature_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.temperatures.type
UAVCANRegisterDeclaration _reg_pub_temperatures_type = {
    .name        = "uavcan.pub.temperatures.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(temperature_subject_type) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) temperature_subject_type,
    .update_hook = NULL,
    .lock        = NULL,
};
