/**
 * @file main.c
 * @author Sajeel Ahmed <sajeel@starcopter.com>, Lasse Fröhner <lasse@starcopter.com>
 * @brief Application Loader
 * @date 2023-04-05
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "main.h"

#include <FreeRTOS.h>
#include <task.h>

#include <sclib/crc.h>
#include <sclib/console.h>
#include <sclib/timekeeper.h>
#include <sclib/registry.h>
#include <canard/fdcan.h>

#include "apps/apps.h"

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

extern void* __VECTOR_TABLE;

ImageHeader_t image_hdr __attribute__((section(".image_hdr"))) = {
    .image_magic  = IMAGE_MAGIC,
    .header_start = (uint32_t*) &image_hdr,
    .vector_table = (uint32_t*) &__VECTOR_TABLE,
    .header_size  = sizeof(ImageHeader_t),
    .image_end    = (uint32_t*) &__image_end,

    .image_hdr_version = eHEADER_VERSION_CURRENT,
    .clean_build       = GIT_CLEAN ? 1 : 0,  // set in Makefile
    .version =
        {
            .major   = VERSION_MAJOR,   // set in Makefile
            .minor   = VERSION_MINOR,   // set in Makefile
            .patch   = VERSION_PATCH,   // set in Makefile
            .commits = VERSION_COMMITS  // set in Makefile
        },
    .min_upgrade_from   = {.major = 0, .minor = 0, .patch = 0, .commits = 0},
    .max_downgrade_from = {.major = VERSION_MAJOR, .minor = VERSION_MINOR, .patch = 255, .commits = 255},
    .target_hardware    = {.major = 'B', .minor = 1},
    .min_hardware       = {.major = 'B', .minor = 1},
    .time_utc           = BUILD_DATE,      // set in Makefile
    .git_sha            = GIT_COMMIT_ARR,  // set in Makefile
    .name               = "com.starcopter.aeric.pssky2",

    .header_crc = 0x12344321,  // populated after linking
};

void init_fdcan(void);
void reinit_fdcan(void);
void generate_boot_id(void);

static void init(void);

int main(void) {
    init_timekeeper();
    init();
    generate_boot_id();

    start_main_task();

    vTaskStartScheduler();

    WTF();
    return 1;
}

static void init(void) {
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM3EN      // Timekeeper
                     | RCC_APB1ENR1_TIM6EN    // ADC Trigger
                     | RCC_APB1ENR1_USART2EN  // Debug Console
                     | RCC_APB1ENR1_FDCANEN;  // CAN
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;    // System Configuration Controller
    RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN         // CRC
                    | RCC_AHB1ENR_DMAMUX1EN   // DMA Multiplexer
                    | RCC_AHB1ENR_DMA1EN      // DMA1
                    | RCC_AHB1ENR_DMA2EN;     // DMA2
    RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN         // Boot ID and other randomness
                    | RCC_AHB2ENR_GPIOAEN     // Port A
                    | RCC_AHB2ENR_GPIOBEN     // Port B
                    | RCC_AHB2ENR_GPIOFEN     // Port F
                    | RCC_AHB2ENR_ADC12EN     // Analog Inputs
                    | RCC_AHB2ENR_DAC1EN;     // Analog Outputs

    SET_BIT(RNG->CR, RNG_CR_RNGEN);  // start random number generator; it requires some time to warm up

    /*
     * Pin Configuration
     * -----------------
     *
     * | Pin  | Signal        | Mode   | OType | OSpeed    | Pull | AF | Peripheral   | Function                          |
     * |------|---------------|--------|-------|-----------|------|---:|--------------|-----------------------------------|
     * | PA0  | `ISKY_ADC`    | Analog |       |           |      |    | `ADC12_IN1`  | Skynode's Current Monitoring      |
     * | PA1  | `IBUCK_ADC`   | Analog |       |           |      |    | `ADC12_IN2`  | Buck Stage Current Monitoring     |
     * | PA2  | `ICHG_ADC`    | Analog |       |           |      |    | `ADC1_IN3`   | Battery Charge Current Monitoring |
     * | PA3  | `IBAT_ADC`    | Analog |       |           |      |    | `ADC1_IN4`   | Battery Current Monitoring        |
     * | PA4  | `IBAT_VREF`   | Output |       |           |      |    | `DAC1_OUT1`  | Reference Battery Current         |
     * | PA5  | `VBAT_ADC`    | Analog |       |           |      |    | `ADC2_IN13`  | Battery Charge Voltage Monitoring |
     * | PA6  | `V48_ADC`     | Analog |       |           |      |    | `ADC2_IN3`   | 48V Supply Voltage Monitoring     |
     * | PA7  | `VSKY_ADC`    | Analog |       |           |      |    | `ADC2_IN4`   | Skynode's Voltage Monitoring      |
     * | PA8  | `SDA`         | AF     | OD    | Low       |      |  4 | `I2C2_SDA`   | I2C SDA to Skynode                |
     * | PA9  | `SCL`         | AF     | OD    | Low       |      |  4 | `I2C2_SCL`   | I2C SCL to Skynode                |
     * | PA10 | `VSKY_EN`     | Output |       | Low       |      |    |              | Enable Boost Converter            |
     * | PA11 | `CAN_RX`      | AF     |       |           |      |  9 | `FDCAN1_RX`  | CAN                               |
     * | PA12 | `CAN_TX`      | AF     | PP    | Medium    |      |  9 | `FDCAN1_TX`  | CAN                               |
     * | PA13 | `SWDIO`       | AF     | PP    | Very High | Up   |  0 | `SWDIO_JTMS` |                                   |
     * | PA14 | `SWCLK`       | AF     |       |           | Down |  0 | `SWCLK_JTCK` |                                   |
     * | PA15 | `CHG_TS`      | Output | OD    |           |      |    |              | Cell Charger Restart              |
     * | PB0  | `CELL_TEMP`   | Analog |       |           |      |    | `ADC1_IN15`  | Cell Temperature Monitoring       |
     * | PB3  | `DEBUG_TX`    | AF     | PP    | Medium    |      |  7 | `USART2_TX`  | Console                           |
     * | PB4  | `CHG_STAT`    | Input  |       |           | Up   |    | `EXTI4`      | Cell Charger State Monitoring     |
     * | PB5  | `CHG_PG`      | Input  |       | Low       | Up   |    |              | Power Good Signal                 |
     * | PB6  | `LED_RED`     | Output | OD    | Low       |      |    |              | LED Output                        |
     * | PB7  | `LED_GREEN`   | Output | OD    | Low       |      |    |              | LED Output                        |
     * | PB8  | `LED_BLUE`    | Output | OD    | Low       |      |    |              | LED Output                        |
     * | PF0  | `VCC_ADC`     | Analog |       |           |      |    | `ADC1_IN10`  | Boost Stage Voltage Monitoring    |
     * | PF1  | `PSTAGE_TEMP` | Analog |       |           |      |    | `ADC2_IN10`  | Temperature Monitoring            |
     * | PG10 | `RST`         |        |       |           |      |    |              | Reset Button                      |
     *
     * */

    GPIOA->AFR[1] = (4ul << GPIO_AFRH_AFSEL8_Pos)           // SDA            AF4: I2C2_SDA
                    | (4ul << GPIO_AFRH_AFSEL9_Pos)         // SCL            AF4: I2C2_SCL
                    | (9ul << GPIO_AFRH_AFSEL11_Pos)        // CAN_RX         AF9: FDCAN1_RX
                    | (9ul << GPIO_AFRH_AFSEL12_Pos)        // CAN_TX         AF9: FDCAN1_TX
                    | (0ul << GPIO_AFRH_AFSEL13_Pos)        // SWDIO          AF0: SWDIO_JTMS
                    | (0ul << GPIO_AFRH_AFSEL14_Pos);       // SWCLK          AF0: SWCLK_JTCK
    GPIOB->AFR[0] = (7ul << GPIO_AFRL_AFSEL3_Pos);          // DEBUG_TX       AF7: USART2_TX
    GPIOA->PUPDR  = (1ul << GPIO_PUPDR_PUPD13_Pos)          // SWDIO_JTMS     Pull-Up
                   | (2ul << GPIO_PUPDR_PUPD14_Pos);        // SWCLK_JTCK     Pull-Down
    GPIOB->PUPDR  = (1ul << GPIO_PUPDR_PUPD4_Pos)           // CHG_STAT       Pull-Up
                   | (1ul << GPIO_PUPDR_PUPD5_Pos);         // CHG_PG         Pull-Up
    GPIOA->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED12_Pos)     // CAN_TX         Medium Speed
                     | (3ul << GPIO_OSPEEDR_OSPEED13_Pos);  // SWDIO          Very-High Speed
    GPIOB->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED3_Pos);     // DEBUG_TX       Medium Speed
    GPIOA->OTYPER  = (1ul << GPIO_ODR_OD8_Pos)              // SDA            Open Drain
                    | (1ul << GPIO_ODR_OD9_Pos)             // SCL            Open Drain
                    | (1ul << GPIO_ODR_OD15_Pos);           // CHG_TS         Open Drain
    GPIOB->OTYPER = (1ul << GPIO_ODR_OD6_Pos)               // LED_RED        Open Drain
                    | (1ul << GPIO_ODR_OD7_Pos)             // LED_GREEN      Open Drain
                    | (1ul << GPIO_ODR_OD8_Pos);            // LED_BLUE       Open Drain
    GPIOA->MODER = (3ul << GPIO_MODER_MODE0_Pos)            // ISKY_ADC       Analog
                   | (3ul << GPIO_MODER_MODE1_Pos)          // IBUCK_ADC      Analog
                   | (3ul << GPIO_MODER_MODE2_Pos)          // ICHG_ADC       Analog
                   | (3ul << GPIO_MODER_MODE3_Pos)          // IBAT_ADC       Analog
                   | (1ul << GPIO_MODER_MODE4_Pos)          // IBAT_VREF      Output
                   | (3ul << GPIO_MODER_MODE5_Pos)          // VBAT_ADC       Analog
                   | (3ul << GPIO_MODER_MODE6_Pos)          // V48_ADC        Analog
                   | (3ul << GPIO_MODER_MODE7_Pos)          // VSKY_ADC       Analog
                   | (2ul << GPIO_MODER_MODE8_Pos)          // SDA            AF
                   | (2ul << GPIO_MODER_MODE9_Pos)          // SCL            AF
                   | (1ul << GPIO_MODER_MODE10_Pos)         // VSKY_EN        Output
                   | (2ul << GPIO_MODER_MODE11_Pos)         // CAN_RX         AF
                   | (2ul << GPIO_MODER_MODE12_Pos)         // CAN_TX         AF
                   | (2ul << GPIO_MODER_MODE13_Pos)         // SWDIO          AF
                   | (2ul << GPIO_MODER_MODE14_Pos)         // SWCLK          AF
                   | (1ul << GPIO_MODER_MODE15_Pos);        // CHG_TS         Output
    GPIOB->MODER = (3ul << GPIO_MODER_MODE3_Pos)            // CELL_TEMP      Analog
                   | (2ul << GPIO_MODER_MODE3_Pos)          // DEBUG_TX       AF
                   | (0ul << GPIO_MODER_MODE4_Pos)          // CHG_STAT       Input
                   | (0ul << GPIO_MODER_MODE5_Pos)          // CHG_PG         Input
                   | (1ul << GPIO_MODER_MODE6_Pos)          // LED_RED        Output
                   | (1ul << GPIO_MODER_MODE7_Pos)          // LED_GREEN      Output
                   | (1ul << GPIO_MODER_MODE8_Pos);         // LED_BLUE       Output
    GPIOF->MODER = (3ul << GPIO_MODER_MODE0_Pos)            // VCC_ADC        Analog
                   | (3ul << GPIO_MODER_MODE1_Pos);         // PSTAGE_TEMP    Analog

    SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI4_PB;
    EXTI->IMR1 |= EXTI_IMR1_IM4;
    EXTI->RTSR1 |= (1ul << EXTI_RTSR1_RT4_Pos);             // enable rising edge trigger (CHG_STAT)
    EXTI->FTSR1 |= (0ul << EXTI_FTSR1_FT4_Pos);             // disable falling edge trigger (CHG_STAT)

    // Turn off all leds
    GPIOB->BSRR = GPIO_BSRR_BS6 | GPIO_BSRR_BS7 | GPIO_BSRR_BS8;

    // Enable VSKY_EN signal to supply voltage to the Skynode
    // TO DO! VSKY_EN shall be enable/disable during the PSSKY redundancy check
    // GPIO locking shall be implemented in a latter phase
    GPIOA->BSRR = GPIO_BSRR_BS10;

    // Set CHG_TS pin LOW to disable charging
    GPIOA->BSRR = GPIO_BSRR_BR15;

    crc_init();
    load_persistent_registers();
    console_init(0);
    init_fdcan();
}

void lock_pssky2_enable_pins(void) {
    CLEAR_BIT(GPIOA->MODER, GPIO_MODER_MODE10);  // VSKY_EN Output (PA10)

    // RM0440 Rev 7, page 365, section 9.4.8 GPIO port configuration lock register
    // Port A lock sequence
    GPIOA->LCKR = GPIO_LCKR_LCKK | GPIO_LCKR_LCK10;
    GPIOA->LCKR = (0ul << GPIO_LCKR_LCKK_Pos) | GPIO_LCKR_LCK10;
    GPIOA->LCKR = GPIO_LCKR_LCKK | GPIO_LCKR_LCK10;
    (void) GPIOA->LCKR;
    ASSERT(GPIOA->LCKR & GPIO_LCKR_LCKK);
}

void init_fdcan(void) {
    // Select PLL "Q" clock (160 MHz) as FDCAN clock source
    MODIFY_REG(RCC->CCIPR, RCC_CCIPR_FDCANSEL, (0b01ul << RCC_CCIPR_FDCANSEL_Pos));
    fdcan_clk = 160000000UL;
}

void reinit_fdcan(void) {
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos) = 1;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos) = 0;

    init_fdcan();
}

uint32_t boot_id[4];

void generate_boot_id(void) {
    for (int i = 0; i < 4; ++i) {
        while (!READ_BIT(RNG->SR, RNG_SR_DRDY)) {
            // wait for random data to be ready
        }
        boot_id[i] = RNG->DR;
    }
}

UAVCANRegisterDeclaration _reg_boot_id = {  // com.starcopter.boot_id
    .name        = "com.starcopter.boot_id",
    .type        = eUAVCANRegisterTypeNatural32Array,
    .mutable     = 0,
    .persistent  = 0,
    .constant    = 1,
    .len         = 4,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) boot_id,
    .update_hook = NULL,
    .lock        = NULL};

void USART2_IRQHandler(void) {
    console_irq_handler();
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char* pcTaskName) {
    LOG_CRITICAL("Stack Overflow for task %s at 0x%08lx", pcTaskName, (uint32_t) xTask);
    console_flush();
    WTF();
}

void vApplicationMallocFailedHook(void) {
    LOG_WARNING("call to pvPortMalloc() failed");
    console_flush();
    HALT_IF_DEBUGGING();
}
