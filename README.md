# Skynodes's Secondary Power Supply (in short PSSKY2) - Hardware Version B.1

[[_TOC_]]
## Processor Configuration
The following table shows the currently active pin configuration.
### Pin Configuration

| Pin  | Signal        | Mode   | OType | OSpeed    | Pull | AF | Peripheral   | Function                          |
|------|---------------|--------|-------|-----------|------|---:|--------------|-----------------------------------|
| PA0  | `ISKY_ADC`    | Analog |       |           |      |    | `ADC12_IN1`  | Skynode's Current Monitoring      |
| PA1  | `IBUCK_ADC`   | Analog |       |           |      |    | `ADC12_IN2`  | Buck Stage Current Monitoring     |
| PA2  | `ICHG_ADC`    | Analog |       |           |      |    | `ADC1_IN3`   | Battery Charge Current Monitoring |
| PA3  | `IBAT_ADC`    | Analog |       |           |      |    | `ADC1_IN4`   | Battery Current Monitoring        |
| PA4  | `IBAT_VREF`   | Output |       |           |      |    | `DAC1_OUT1`  | Reference Battery Current         |
| PA5  | `VBAT_ADC`    | Analog |       |           |      |    | `ADC2_IN13`  | Battery Charge Voltage Monitoring |
| PA6  | `V48_ADC`     | Analog |       |           |      |    | `ADC2_IN3`   | 48V Supply Voltage Monitoring     |
| PA7  | `VSKY_ADC`    | Analog |       |           |      |    | `ADC2_IN4`   | Skynode's Voltage Monitoring      |
| PA8  | `SDA`         | AF     | OD    | Low       |      |  4 | `I2C2_SDA`   | I2C SDA to Skynode                |
| PA9  | `SCL`         | AF     | OD    | Low       |      |  4 | `I2C2_SCL`   | I2C SCL to Skynode                |
| PA10 | `VSKY_EN`     | Output |       | Low       |      |    |              | Enable Boost Converter            |
| PA11 | `CAN_RX`      | AF     |       |           |      |  9 | `FDCAN1_RX`  | CAN                               |
| PA12 | `CAN_TX`      | AF     | PP    | Medium    |      |  9 | `FDCAN1_TX`  | CAN                               |
| PA13 | `SWDIO`       | AF     | PP    | Very High | Up   |  0 | `SWDIO_JTMS` |                                   |
| PA14 | `SWCLK`       | AF     |       |           | Down |  0 | `SWCLK_JTCK` |                                   |
| PA15 | `CHG_TS`      | Output | OD    |           |      |    |              | Cell Charger Restart              |
| PB0  | `CELL_TEMP`   | Analog |       |           |      |    | `ADC1_IN15`  | Cell Temperature Monitoring       |
| PB3  | `DEBUG_TX`    | AF     | PP    | Medium    |      |  7 | `USART2_TX`  | Console                           |
| PB4  | `CHG_STAT`    | Input  |       |           | Up   |    | `EXTI4`      | Cell Charger State Monitoring     |
| PB5  | `CHG_PG`      | Input  |       | Low       | Up   |    |              | Power Good Signal                 |
| PB6  | `LED_RED`     | Output | OD    | Low       |      |    |              | LED Output                        |
| PB7  | `LED_GREEN`   | Output | OD    | Low       |      |    |              | LED Output                        |
| PB8  | `LED_BLUE`    | Output | OD    | Low       |      |    |              | LED Output                        |
| PF0  | `VCC_ADC`     | Analog |       |           |      |    | `ADC1_IN10`  | Boost Stage Voltage Monitoring    |
| PF1  | `PSTAGE_TEMP` | Analog |       |           |      |    | `ADC2_IN10`  | Temperature Monitoring            |
| PG10 | `RST`         |        |       |           |      |    |              | Reset Button                      | 

### Subsystems
#### USART2
Debug console (logging).

#### TIM3 
Timekeeper clock and FDCAN hardware timestamping.

#### TIM4
PWM output signals for status LEDs.

#### FDCAN1
CAN (Cyphal) interface, handled by the protocol stack.

#### CRC
Hardware CRC unit.

#### RNG
Random number generator, currently only used to generate a random boot ID exposed via the `com.starcopter.boot_id` Cyphal register.

#### ADC1, ADC2
Analog inputs for temperature measurement, buck and boost converter voltage and current monitoring, battery charge voltage and current monitoring, and on board voltage level supervision.


### Interrupts

The STM32G4 uses 16 interrupt priorities, where a lower numerical value means higher priority.
Interrupts running at priority 4 and lower are safe to call FreeRTOS API functions,
interrupts running at priority 3 and higher are above FreeRTOS.

> **TODO**: this table may be incomplete or incorrect

| Interrupt    | Priority | Purpose                                         |
| ------------ | -------: | ----------------------------------------------- |
| `SVC`        |        0 | starting FreeRTOS                               |
| `FDCAN1_IT1` |        6 | notifying the Transport task of bus events      |
| `TIM3`       |        8 | Timekeeper                                      |
| `USART2`     |       14 | TX FIFO empty, copy characters from ring buffer |
| `SysTick`    |       15 | FreeRTOS scheduler tick                         |
| `PendSV`     |       15 | something FreeRTOS                              |

### FreeRTOS Tasks

FreeRTOS is [configured](src/FreeRTOSConfig.h) with 8 different task priorities, mirroring the Cyphal transfer
priorities. Not all of these priorities are currently in use, however. A higher numerical value means higher priority.

> **TODO**: this table may be incomplete or incorrect

| Task           | Priority | File               | Purpose                                                          |
| -------------- | -------: | ------------------ | ---------------------------------------------------------------- |
| Transport      |        6 | [transport.c][]    | Cyphal (via CAN FD) transport worker task, handles communication |
| Timer Svc      |        5 | [timers.c][]       | Service task for [FreeRTOS Timers][]                             |
| Node Monitor   |        2 | [node-monitor.c][] | Network monitor, tracks other nodes on the network               |
| GetInfo xxx/yy |        2 | [node.c][]         | Spawned to handle uavcan.node.GetInfo requests                   |
| xCmd xxx/yy    |        2 | [node.c][]         | Spawned to handle uavcan.node.ExecuteCommand requests            |
| List xxx/yy    |        1 | [registry.c][]     | Spawned to handle uavcan.register.List requests                  |
| Access xxx/yy  |        1 | [registry.c][]     | Spawned to handle uavcan.register.Access requests                |
| Main           |        1 | [main.c][]         | Main task, started first and responsible to start other tasks    |
| IDLE           |        0 | [tasks.c][]        | FreeRTOS background task, see [docs][idle task] for details      |

[main.c]: src/apps/main.c
[node-monitor.c]: src/apps/node-monitor.c
[pwm-input.c]: src/apps/pwm-input.c
[tasks.c]: submodules/freertos/kernel/tasks.c
[transport.c]: submodules/shared-utilities/canard/transport.c
[node.c]: submodules/shared-utilities/canard/node.c
[registry.c]: submodules/shared-utilities/sclib/registry.c
[timers.c]: submodules/freertos/kernel/timers.c
[freertos timers]: https://www.freertos.org/RTOS-software-timer.html
[idle task]: https://www.freertos.org/RTOS-idle-task.html

## Bootloader and DCB
The platform shall be provisioned under the name `com.starcopter.aeric.pssky2` along with the hardware release `A.2` using the following command from the terminal. 
```shell
pyric device provision --bootloader ../bootloader/ --app ../app-loader/  --app . com.starcopter.aeric.pssky2 A.2
```

After resoldering a processor from an old PCB to a new one, the processor will need to be re-provisioned.

