/**
 * @file ina2xx.c
 * @author Lasse Fröhner (lasse@starcopter.com)
 * @brief INA238 Emulation
 * @version 0.2
 * @date 2023-05-12
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 *
 * ATTENTION: This implementation requires the following PX4 parameter settings:
 *
 * | PX4 Parameter     |  Value |
 * | ----------------- | -----: |
 * | `SENSE_EN_INA238` |      1 |
 * | `INA238_CURRENT`  | 327.68 |
 * | `INA238_SHUNT`    | 0.0003 |
 *
 *
 * INA238 Register Maps
 * --------------------
 *
 * | Addr   | Acronym           | Register Name                    | Size |
 * | ------ | ----------------- | -------------------------------- | ---: |
 * | `0x00` | `CONFIG`          | Configuration                    |   16 |
 * | `0x01` | `ADC_CONFIG`      | ADC Configuration                |   16 |
 * | `0x02` | `SHUNT_CAL`       | Shunt Calibration                |   16 |
 * | `0x04` | `VSHUNT`          | Shunt Voltage Measurement        |   16 |
 * | `0x05` | `VBUS`            | Bus Voltage Measurement          |   16 |
 * | `0x06` | `DIETEMP`         | Temperature Measurement          |   16 |
 * | `0x07` | `CURRENT`         | Current Result                   |   16 |
 * | `0x08` | `POWER`           | Power Result                     |   24 |
 * | `0x0b` | `DIAG_ALRT`       | Diagnostic Flags and Alert       |   16 |
 * | `0x0c` | `SOVL`            | Shunt Overvoltage Threshold      |   16 |
 * | `0x0d` | `SUVL`            | Shunt Undervoltage Threshold     |   16 |
 * | `0x0e` | `BOVL`            | Bus Overvoltage Threshold        |   16 |
 * | `0x0f` | `BUVL`            | Bus Undervoltage Threshold       |   16 |
 * | `0x10` | `TEMP_LIMIT`      | Temperature Over-Limit Threshold |   16 |
 * | `0x11` | `PWR_LIMIT`       | Power Over-Limit Threshold       |   16 |
 * | `0x3e` | `MANUFACTURER_ID` | Manufacturer ID (`0x5449`)       |   16 |
 * | `0x3f` | `DEVICE_ID`       | Device ID (`0x2381`)             |   16 |
 *
 *
 * Random Facts
 * ------------
 *
 * - Register bytes are sent most-significant byte first, followed by the least significant byte
 *
 */

#include <main.h>
#include "apps.h"

#include <timers.h>
#include <sclib/registry.h>
#include <starcopter/aeric/motherboard/INAEmulatorMetrics_0_1.h>

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

#define INA_CONFIG_REG          0x00u
#define INA_ADC_CONFIG_REG      0x01u
#define INA_SHUNT_CAL_REG       0x02u
#define INA_VSHUNT_REG          0x04u
#define INA_VBUS_REG            0x05u
#define INA_DIETEMP_REG         0x06u
#define INA_CURRENT_REG         0x07u
#define INA_POWER_REG           0x08u
#define INA_DIAG_ALRT_REG       0x0bu
#define INA_SOVL_REG            0x0cu
#define INA_SUVL_REG            0x0du
#define INA_BOVL_REG            0x0eu
#define INA_BUVL_REG            0x0fu
#define INA_TEMP_LIMIT_REG      0x10u
#define INA_PWR_LIMIT_REG       0x11u
#define INA_MANUFACTURER_ID_REG 0x3eu
#define INA_DEVICE_ID_REG       0x3fu

#define INA_DIETEMP_DIETEMP_Pos 4
#define INA_DIETEMP_DIETEMP_Msk (0x0fffu << INA_DIETEMP_DIETEMP_Pos)
#define INA_DIETEMP_DIETEMP     INA_DIETEMP_DIETEMP_Msk

#define INA_VBUS_LSB    (3.125e-3f)  // 3.125 mV / LSB
#define INA_DIETEMP_LSB (125e-3f)    // 125 mK / LSB
#define INA_CURRENT_LSB (10e-3f)     // 10 mA / LSB

#define INA_MANUFACTURER_ID (0x5449u)  // "TI"
#define INA_DEVICE_ID       (0x2381u)  // INA238, Rev. 1

#define INA_7BIT_ADDR 0x45u

// ================================================== PRIVATE API ======================================================

volatile uint16_t config_register_write_count     = 0;
volatile uint16_t adc_config_register_write_count = 0;
volatile uint16_t shunt_cal_register_write_count  = 0;
volatile uint16_t _other_register_write_count     = 0;

volatile uint16_t config_register_read_count          = 0;
volatile uint16_t adc_config_register_read_count      = 0;
volatile uint16_t shunt_cal_register_read_count       = 0;
volatile uint16_t vbus_register_read_count            = 0;
volatile uint16_t dietemp_register_read_count         = 0;
volatile uint16_t current_register_read_count         = 0;
volatile uint16_t power_register_read_count           = 0;
volatile uint16_t manufacturer_id_register_read_count = 0;
volatile uint16_t device_id_register_read_count       = 0;
volatile uint16_t _other_register_read_count          = 0;

volatile uint16_t ina_state_machine_reset_count = 0;

void init_i2c(void) {
    // necessary prerequisite: the GPIO ports have been configured already

    // enable I2C2 peripheral
    PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_I2C2EN_Pos) = 1;
    // select HSI16 as I2C2 Clock
    MODIFY_REG(RCC->CCIPR, RCC_CCIPR_I2C2SEL, (0b10ul << RCC_CCIPR_I2C2SEL_Pos));

    // RM0440 Rev 7, page 1864ff, figures 633, 636
    //  1. Clear PE bit in I2C_CR1
    //  2. Configure ANFOFF and DNF[3:0] in I2C_CR1
    //  3. Configure PRESC[3:0], SDADEL[3:0], SCLDEL[3:0], SCLH[7:0], SCLL[7:0] in I2C_TIMINGR
    //  4. Configure NOSTRETCH in I2C_CR1
    //  5. Set PE bit in I2C_CR1
    //  6. Clear {OA1EN, OA2EN} in I2C_OAR1 and I2C_OAR2
    //  7. Configure {OA1[9:0], OA1MODE, OA1EN, OA2[6:0], OA2MSK[2:0], OA2EN, GCEN}
    //  8. Configure SBC in I2C_CR1
    //  9. Enable interrupts and/or DMA in I2C_CR1

    PERIPH_BIT(I2C2->CR1, I2C_CR1_PE_Pos) = 0;
    I2C2->OAR1                            = I2C_OAR1_OA1EN | (INA_7BIT_ADDR << 1);
    I2C2->OAR2                            = 0;
    I2C2->CR1                             = 0;
    // RM0440 Rev 7, page 1888, Table 386. Examples of timings settings for fI2CCLK = 16 MHz, Standard-mode 100 kHz
    I2C2->TIMINGR = (3ul << I2C_TIMINGR_PRESC_Pos)     // 4 MHz
                  | (4ul << I2C_TIMINGR_SCLDEL_Pos)    // 1.25 µs
                  | (2ul << I2C_TIMINGR_SDADEL_Pos)    // 500 ns
                  | (0x0ful << I2C_TIMINGR_SCLH_Pos)   // 4 µs
                  | (0x13ul << I2C_TIMINGR_SCLL_Pos);  // 5 µs

    I2C2->CR1 = I2C_CR1_RXIE    // receive buffer not empty interrupt
              | I2C_CR1_TXIE    // transmit buffer interrupt status
              | I2C_CR1_ADDRIE  // address match interrupt
              | I2C_CR1_PE;     // Peripheral Enable

    NVIC_SetPriority(I2C2_EV_IRQn, InterruptPriorityINA2xx);
    NVIC_EnableIRQ(I2C2_EV_IRQn);
}

// https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
static uint_fast8_t count_bits_set(uint32_t value) {
    uint_fast8_t count;  // count accumulates the total bits set in value
    for (count = 0; value; count++)
        value &= value - 1;  // clear the least significant bit set

    return count;
}

void delayed_init_i2c(TimerHandle_t th) {
    if (!th) return;

    init_i2c();
    LOG_INFO("INA238 emulation on 0x%02x initialized", INA_7BIT_ADDR);
    xTimerDelete(th, portMAX_DELAY);
}

// uavcan.pub.ina2xx_metrics: starcopter.aeric.motherboard.INAEmulatorMetrics.0.1
volatile uint16_t ina2xx_metrics_subject_id = SUBJECT_ID_DEFAULT;
static const char ina2xx_metrics_subject_type[] =
    starcopter_aeric_motherboard_INAEmulatorMetrics_0_1_FULL_NAME_AND_VERSION_;

// uavcan.pub.ina2xx_metrics.id
UAVCANRegisterDeclaration _reg_pub_ina2xx_metrics_id = {
    .name        = "uavcan.pub.ina2xx_metrics.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &ina2xx_metrics_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.ina2xx_metrics.type
UAVCANRegisterDeclaration _reg_pub_ina2xx_metrics_type = {
    .name        = "uavcan.pub.ina2xx_metrics.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(ina2xx_metrics_subject_type) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) ina2xx_metrics_subject_type,
    .update_hook = NULL,
    .lock        = NULL,
};

static void send_ina_metrics(TimerHandle_t th __unused) {
    const uint16_t subject_id = ina2xx_metrics_subject_id;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;

    // manual "serialized" representation of a starcopter.aeric.motherboard.INAEmulatorMetrics.0.1
    const struct __packed INAEmulatorMetrics01 {
        uint16_t manfid, devid;
        uint16_t config_w, adc_config_w, shunt_cal_w, other_w;
        uint16_t config_r, adc_config_r, shunt_cal_r, vbus_r, dietemp_r, current_r, power_r, manufacturer_id_r,
            device_id_r, other_r;
        uint16_t state_machine_reset_count;
    } metrics = {.manfid                    = INA_MANUFACTURER_ID,
                 .devid                     = INA_DEVICE_ID,
                 .config_w                  = config_register_write_count,
                 .adc_config_w              = adc_config_register_write_count,
                 .shunt_cal_w               = shunt_cal_register_write_count,
                 .other_w                   = _other_register_write_count,
                 .config_r                  = config_register_read_count,
                 .adc_config_r              = adc_config_register_read_count,
                 .shunt_cal_r               = shunt_cal_register_read_count,
                 .vbus_r                    = vbus_register_read_count,
                 .dietemp_r                 = dietemp_register_read_count,
                 .current_r                 = current_register_read_count,
                 .power_r                   = power_register_read_count,
                 .manufacturer_id_r         = manufacturer_id_register_read_count,
                 .device_id_r               = device_id_register_read_count,
                 .other_r                   = _other_register_read_count,
                 .state_machine_reset_count = ina_state_machine_reset_count};
    static_assert(sizeof(metrics) ==
                      starcopter_aeric_motherboard_INAEmulatorMetrics_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_,
                  "Something's wrong");

    static uint_fast8_t          transfer_id = 0;
    const CanardTransferMetadata meta        = {.priority       = CanardPrioritySlow,
                                                .transfer_kind  = CanardTransferKindMessage,
                                                .port_id        = subject_id,
                                                .remote_node_id = CANARD_NODE_ID_UNSET,
                                                .transfer_id    = transfer_id++};

    node_publish(450000ul, &meta, starcopter_aeric_motherboard_INAEmulatorMetrics_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_,
                 (uint8_t*) &metrics);
}

// =================================================== PUBLIC API ======================================================

void init_ina2xx_emulation(void) {
    TimerHandle_t _timer = xTimerCreate("delayed", pdMS_TO_TICKS(250), pdFALSE, NULL, delayed_init_i2c);
    xTimerStart(_timer, portMAX_DELAY);
    TimerHandle_t _metrics_timer = xTimerCreate("INA metrics", pdMS_TO_TICKS(500), pdTRUE, NULL, send_ina_metrics);
    xTimerStart(_metrics_timer, portMAX_DELAY);
}

// =============================================== INTERRUPT HANDLERS ==================================================

void I2C2_EV_IRQHandler(void) {
    static enum I2CSlaveState { ina_idle, ina_write_addr, ina_write_data, ina_read_data } ina_i2c_state = ina_idle;
    static struct INA238State {
        uint_fast16_t config_register;
        uint_fast16_t adc_config_register;
        uint_fast16_t shunt_cal_register;
        // uint_fast16_t diag_alrt_register;
        // uint_fast16_t sovl_register;
        // uint_fast16_t suvl_register;
        // uint_fast16_t bovl_register;
        // uint_fast16_t buvl_register;
        // uint_fast16_t temp_limit_register;
        // uint_fast16_t pwr_limit_register;
    } ina_state;

    static uint_fast8_t       addr = 0, low_byte = 0;
    static uint_fast16_t      reg;
    static volatile uint16_t* ctr = NULL;  // static pointer to volatile counter variable

    const uint32_t isr   = I2C2->ISR;
    const uint32_t FLAGS = I2C_ISR_ADDR | I2C_ISR_RXNE | I2C_ISR_TXIS;

    ASSERT(count_bits_set(isr & FLAGS) == 1);

    if (isr & I2C_ISR_ADDR) {
        if (isr & I2C_ISR_DIR) {
            // read access
            switch (addr) {
                case INA_CONFIG_REG:
                    reg = ina_state.config_register;
                    ctr = &config_register_read_count;
                    break;
                case INA_ADC_CONFIG_REG:
                    reg = ina_state.adc_config_register;
                    ctr = &adc_config_register_read_count;
                    break;
                case INA_SHUNT_CAL_REG:
                    reg = ina_state.shunt_cal_register;
                    ctr = &shunt_cal_register_read_count;
                    break;
                case INA_VBUS_REG:
                    reg = get_cell_voltage() / INA_VBUS_LSB;
                    ctr = &vbus_register_read_count;
                    break;
                case INA_DIETEMP_REG:
                    reg = ((int16_t) (get_core_temperature() / INA_DIETEMP_LSB)) << INA_DIETEMP_DIETEMP_Pos;
                    ctr = &dietemp_register_read_count;
                    break;
                case INA_CURRENT_REG:
                    reg = 0;
                    ctr = &current_register_read_count;
                    break;
                case INA_POWER_REG:
                    reg = 0;
                    ctr = &power_register_read_count;
                    break;
                case INA_MANUFACTURER_ID_REG:
                    reg = INA_MANUFACTURER_ID;
                    ctr = &manufacturer_id_register_read_count;
                    break;
                case INA_DEVICE_ID_REG:
                    reg = INA_DEVICE_ID;
                    ctr = &device_id_register_read_count;
                    break;
                default:
                    reg = 0;
                    ctr = &_other_register_read_count;
                    break;
            }
            ina_i2c_state = ina_read_data;
            I2C2->ISR     = I2C_ISR_TXE;  // flush data register
            low_byte      = 0;
        } else {
            // write access
            ina_i2c_state = ina_write_addr;
        }
        I2C2->ICR = I2C_ICR_ADDRCF;
    } else if (isr & I2C_ISR_RXNE) {
        switch (ina_i2c_state) {
            case ina_write_addr:
                addr          = I2C2->RXDR;
                ina_i2c_state = ina_write_data;
                low_byte      = 0;
                break;
            case ina_write_data:
                ((uint8_t*) &reg)[low_byte ? 0 : 1] = I2C2->RXDR;
                if (low_byte) {
                    // data written
                    switch (addr) {
                        case INA_CONFIG_REG:
                            ina_state.config_register = reg;
                            config_register_write_count++;
                            break;
                        case INA_ADC_CONFIG_REG:
                            ina_state.adc_config_register = reg;
                            adc_config_register_write_count++;
                            break;
                        case INA_SHUNT_CAL_REG:
                            ina_state.shunt_cal_register = reg;
                            shunt_cal_register_write_count++;
                            break;
                        default:
                            _other_register_write_count++;
                            break;
                    }
                    ina_i2c_state = ina_idle;
                } else {
                    low_byte = 1;
                    // wait for second byte
                }
                break;
            default:
                // ignore and reset
                (void) I2C2->RXDR;
                ina_state_machine_reset_count++;
                ina_i2c_state = ina_idle;
                break;
        }
    } else if (isr & I2C_ISR_TXIS) {
        if (ina_i2c_state == ina_read_data) {
            I2C2->TXDR = ((uint8_t*) &reg)[low_byte ? 0 : 1];
            if (low_byte) {
                // data written
                (*ctr)++;
                ina_i2c_state = ina_idle;
            } else {
                low_byte = 1;
                // wait for second byte
            }
        } else {
            // ignore and reset
            I2C2->TXDR = 0;
            ina_state_machine_reset_count++;
            ina_i2c_state = ina_idle;
        }
    }
}
